Assunto da reunião

Hora da reunião:

Grupo presente na reunião:

Hora de entrega da ata:

Quem criou a ata:

Tempo despendido (em pommodoros):







/label Reunioes
/spend [por tempo aqui]
/estimate [por tempo aqui]

[↓ APAGAR AO CRIAR O ISSUE ↓]
ex.: /spend 20m (20 minutos)
     /spend 1h  (1 hora)
     /estimate 20m (20 minutos)
     /estimate 1h  (1 hora)

     /estimate -> usar no inicio para quanto tempo pensamos que vamos gastar
     /spend -> usar para ir adicionando à medida que se vai gastando tempo)

No grupo presente colocar "todos" no caso de ser uma reunião geral
