***** 1 *****

(1.A) NOT DONE | André Queirós - ? (Ninguém a verificar (PRECISA DE SUBSTITUTO!))
(1.B) DONE | Nathalie Jennings - Dumitru Brailean
(1.C) NOT DONE | Luis Gonçalves - Nathalie Jennings (Verificação não feita)
(1.D) NOT DONE | João Santos - Miguel Faria (User story não criado / Verificação não feita)
(1.E) DONE | Isabel Carvalho - Bruno Marques (Formato errado no user story)
(1.F) DONE | Susana Batista - Nathalie Jennings 
(1.G) DONE | Dumitru Brailean - Ana Gil (Verificação não feita)
(1.H) DONE | Nathalie Jennings - Pedro Letra

***** 2 *****

(2.A) DONE | Pedro Letra - Luís Gonçalves
(2.B) DONE | Beatriz Ferreira - Susana Batista
(2.C) DONE | Filipa Nogueira - Bárbara Teixeira
(2.D) NOT DONE | João Silva - João Matos (Verificação não feita)
(2.E) DONE | Bárbara Teixeira - Filipa Nogueira

***** 3 ******
	
(3.A) DONE | Luís Gonçalves - Beatriz Ferreira
(3.B) DONE | Miguel Faria - Alexandre van Velze
(3.C) DONE | João Santos - Isabel Carvalho

***** 4 *****

(4.1.A) DONE | Pedro Martins - Mário Lemos
(4.1.B) DONE | Mário Lemos - Pedro Martins
(4.1.C) DONE | João Melo - João Lopes
(4.1.D) DONE | Fábio Vaqueiro - João Melo
(4.2.A) DONE | Francisco Pires - João Santos
(4.2.B) DONE | Alexandre van Velze - António Correia
(4.2.C) DONE | João Matos - Francisco Pires
(4.2.D) DONE | João Lopes - Miguel Faria
(4.2.E) DONE | Duarte Santa - Dumitry Brailean (User story não completo / Duarte saiu do grupo portanto é preciso substituto)

***** 5 *****

(5.A) DONE | David Forte - Fábio Vaqueiro
(5.B) DONE | Ana Gil - David Forte

***** 6 *****

(6.A) DONE | Bruno Marques - Ana Gil (Verificação não feita)
(6.B) NOT DONE | Francisco Pires - Miguel Faria (User story não criado / Verificação não feita)
(6.C) DONE | António Correia - João Silva 


//João Matos fez apenas 4 issues, sem verificação.
