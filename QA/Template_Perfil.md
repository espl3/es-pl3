Nome:

Número de estudante:

Email:

ID no GitLab:

Cargos: 

Contribuições:
ARCH:
-
-
-
-
-
-
-
-(...)

DES:
-
-

DEV:
-
-

PM:
-
-

PROC:
-
-

PROD:
-
-

QA:
-
-

Relatório:
-
-

REQ:
-
-

TST:
-
-

Outras tarefas:
(Issue #???) 

Papel principal no projeto:
(ex: Fiz parte do controlo de qualidade, fui Front_end/ Back-end developer, etc. - O que fiz durante o projeto)
  

--NOTA: APAGAR O QUE ESTÁ ABAIXO APÓS CRIAR O FICHEIRO!--

Nas Contribuições: 
Seguir exemplo:

PM: 
- Criação do ficheiro de prefil (Issue #???)

Apagar as categorias cuja as contribuições são nulas;
Caso haja mais do que sete contribuições numa categoria, colocar reticências.

Em Outras tarefas:
Colocar os issues (nome e id do issue) que não estejam relacionado com qualquer uma das categorias do repositório.

No Papel principal:
Explicar o que fez durante o projeto.
  