Requisito a testar (Identificador e descrição):

Desenvolvedor do requisito:

Passos de teste:

-
-
-

Critérios que falta implementar/mal implementados:

-
-
-

Notas finais (opcional):

Tempo despendido (em pommodoros):







/label Testes
/spend [por tempo aqui]
/estimate [por tempo aqui]

[↓ APAGAR AO CRIAR O ISSUE ↓]
ex.: /spend 20m (20 minutos)
     /spend 1h  (1 hora)
     /estimate 20m (20 minutos)
     /estimate 1h  (1 hora)

     /estimate -> usar no inicio para quanto tempo pensamos que vamos gastar
     /spend -> usar para ir adicionando à medida que se vai gastando tempo
