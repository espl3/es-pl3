Identificador do Requisito:

Nome:


Etapas:
-
-
-


Finalidade:


Tempo despendido (em pommodoros):







/label Requisitos
/spend [por tempo aqui]
/estimate [por tempo aqui]

[↓ APAGAR AO CRIAR O ISSUE ↓]
ex.: /spend 20m (20 minutos)
     /spend 1h  (1 hora)
     /estimate 20m (20 minutos)
     /estimate 1h  (1 hora)

     /estimate -> usar no inicio para quanto tempo pensamos que vamos gastar
     /spend -> usar para ir adicionando à medida que se vai gastando tempo

(se for preciso adicionar mais traços nas etapas)
(etapas consiste nas ações do utilizador e do sistema)

ex do campo 'Etapas':
User-story exemplo: Como utilizador quero poder fazer reset à minha password para poder usar o sistema mesmo que a esqueça

- Utilizador seleciona opção de recuperação de password na página de login
- Sistema pede endereço de email pré-existente
- Utilizador deve introduzir endereço de mail válido
- Sistema envia mensagem com link único para esse email depois de o validar
- Utilizador acede à página e introduz nova password
- Sistema associa nova password à conta do email enviado, e utilizador reencaminhado para página de login
- Utilizador visualiza página de login
