Identificador:

Nome do Requisito:

Alterações na Descrição:
(antigo)
-
-

(novo)

-
-

Alterações nos Critérios de Aceitação Funcionais:
(antigo)
-
-

(novo)

-
-

Alterações nos Critérios de Aceitação Não Funcionais:
(antigo)
-
-

(novo)

-
-

Tempo despendido (em pommodoros):







/label Verificacao_de_Requisitos
/spend [por tempo aqui]
/estimate [por tempo aqui]

[↓ APAGAR AO CRIAR O ISSUE ↓]
ex.: /spend 20m (20 minutos)
     /spend 1h  (1 hora)
     /estimate 20m (20 minutos)
     /estimate 1h  (1 hora)

     /estimate -> usar no inicio para quanto tempo pensamos que vamos gastar
     /spend -> usar para ir adicionando à medida que se vai gastando tempo