Autor: Andre Queiros
Verificado por: ?

Deverá ser possivel escolher o repositório GitLab a analisar.
Identificador do Requisito: 1.A

Nome: Deverá ser possível escolher o repositório GitLab a analisar.
Descrição: Deverá haver uma forma fácil e objetiva de escolher qual o repositório a ser analisado.

Critérios de Aceitação Funcionais:
- Fácil acesso á página de acesso aos repositórios.
- Devo conseguir observar todos os repositórios independentemente que haja um elevado número dos mesmos.

Critérios de Aceitação Não Funcionais:
- Deverão todos os autores ter acesso aos repositórios?- Poderei voltar rapidamente atrás, refazendo a minha escolha, sendo que me enganei na escolha do repositório?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Nathalie Jennings
Verificado por: Dumitru Brailean

Deverá ser possível escolher o branch que está ser visualizado. Por defeito será o main branch (a visualização por branch será um requisito avançado).
Identificador do Requisito: 1.B

Nome: Deverá ser possível escolher o branch que está ser visualizado. Por defeito será o main branch (a visualização por branch será um requisito avançado).
Descrição: Como developer, preciso ter a opção de escolher o branch a ser visualizado para isolar o desenvolvimento sem afetar outras ramificações no repositório

Critérios de Aceitação Funcionais:
- O nome do branch a ser visualizado deverá estar sempre presente no écran

Critérios de Aceitação Não Funcionais:
- É possível comparar dois branch lado a lado?
- É possível trocar as preferencias da visualização do branch por defeito(main)?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Luis Goncalves
Verificado por: Nathalie Jennings (Por fazer)

A visualização temporal (linha de tempo) deverá ser uniforme.
Identificador do Requisito: 1.C

Nome: A visualização temporal (linha de tempo) deverá ser uniforme, i.e. as unidades de tempo devem corresponder ao mesmo comprimento na sua visualização.

Descrição: Como membro ativo de um projeto, devo ver os intervalos temporais nas linhas de tempo de forma uniforme, para facilitar a navegação e interpretação da timeline.

Critérios de Aceitação Funcionais:

A timeline não deve ocupar somente a largura da janela do browser. Deve ocupar uma janela navegável na horizontal (ou vertical?), para que a informação tenha o espaço adequado à sua leitura.
A timeline deverá estar dividida em segmentos iguais de tempo. Cada um dos segmentos deve corresponder a 1 semana de um mês (ou 1 dia?). Cada uma dessas semanas deve estar dividida pelos seus dias.
Criterios de Aceitação Nao Funcionais:

A timeline deve estar sempre atualizada. [(?)a sua visualização começa momento do acesso, ou numa unidade inteira de tempo?]
A timeline deve ser capaz de apresentar um projeto inteiro independentemente da sua duração.

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Isabel Carvalho
Verificado por: Bruno Marques

Os eventos na linha temporal devem estar organizado por commit instants.
Identificador do Requisito: 1.E

Nome: Os eventos na linha temporal devem estar organizado por commit instants.
Descricao: Como utilizador ativo do projeto devo estruturar os eventos na linha temporal com a devida utilização de commit instants.

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Susana Pires
Verificado por: Nathalie Jennings

Deverá existir funcionalidade ‘back’, i.e. regressar à vista anteriormente apresentada sem ter de reinserir as opções que levaram à sua visualização
Identificador do Requisito: 1.F

Nome: Deverá existir funcionalidade ‘back’, i.e. regressar à vista anteriormente apresentada sem ter de reinserir as opções que levaram à sua visualização.
Descricao: Como utilizador quero ter uma funcionalidade ‘back’ para ter facilidade no acesso ao site.

Criterios de Aceitacao Funcionais:
- Terá de haver um botão que volte à página imediatamente anterior.
- Guardar as opções inseridas.

Criterios de Aceitacao Nao Funcionais:
- As opções são guardadas para uma próxima utilização a longo prazo?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Dumitru Brailean
Verificado por: Ana Gil (Por fazer)

Cada novo acesso à dashboard deve envolver a recolha dos dados de atividade mais recente no repositório.
Identificador do Requisito: 1.G

Nome: Deverá envolver a recolha dos dados de atividade mais recente no repositório em cada novo acesso à dashboard.
Descricao: Como developer, preciso ter a opção para criar cada novo acesso à dashboard, para que possa envolver a recolha dos dados de atividade mais recente no repositório.

Criterios de Aceitacao Funcionais:
- Função para criar novo acesso à dashboard.
- Envolve a recolha de dados mais recente no repositório.
- Guardar as opções inseridas.

Criterios de Aceitacao Nao Funcionais:
- A recolha de dados serão mesmo recentes?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Nathalie Jennings
Verificado por: Pedro Letra

O momento da última atualização dos dados deverá estar sempre presente no écran
Identificador do Requisito: 1.H

Nome: O momento da última atualização dos dados deverá estar sempre presente no écran (e.g. “Última atualização há 3 minutos”).
Descrição: Como membro ativo, necessito visualizar o momento da última atualização dos dados para ter certeza de que tenho a versão atual dos dados.

Criterios de Aceitacao Funcionais:
- Unidade de tempo sempre atualizada

Criterios de Aceitacao Nao Funcionais:
- Será possível visualizar a última atualização em todas as funções do site?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Pedro Letra
Verificado por: Luis Goncalves

Deverá ser apresentada a árvore de ficheiros do repositório
Identificador do Requisito: 2.A

Nome: Deverá ser apresentada a árvore de ficheiros do repositório
Descricao: Como membro ativo de um projeto, devo poder visualizar e navegar pela árvore de ficheiros do repositório, de forma a que possa facilmente aceder ao conteúdo dos ficheiros.

Criterios de Aceitacao Funcionais:
- Por todos os ficheiros e pastas presentes no repositório, deve ser possível ver o seu nome, extensão, mensagem do último commit e a data da sua última atualização no repositório.
- Deve ser possível aceder a todos os ficheiro e pastas na árvore de ficheiros.
- Deve ser possível alterar o branch ao qual estamos a aceder.
- Deve ser possível fazer uma procura relativa aos ficheiros e pastas presentes no repositório.
- Ao navegar pelas pastas deve ser possível retornar à pasta anterior.
- A mensagem e a data do último commit deve ser apresentado.

Critérios de Aceitacao Nao Funcionais:
- A árvore de ficheiros deve atualizar sempre que for feita uma alteração ao repositório.

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Beatriz Ferreira
Verificado por: Susana Pires

Os ficheiros presentes em cada diretoria/pasta deverão poder ser visíveis (nome e extensão dos ficheiros)
Identificador do Requisito: 2.B

Nome: Os ficheiros presentes em cada diretoria/pasta deverão poder ser visíveis (nome e extensão dos ficheiros)
Descrição: Como utilizador quero que o nome e extensão dos ficheiros sejam sempre visíveis para tornar o acesso aos ficheiros mais fácil e rápido.

Critérios de Aceitação Funcionais:
- Nome e extensão dos ficheiros sempre visíveis;
- O nome dos ficheiros deve ser curto e direto;

Critérios de Aceitação Não Funcionais:
- Será possível alterar a ordem do nome dos ficheiros?
- Será possível ordenar alfabeticamente os ficheiros?
- Será possível agrupar os ficheiros por extensões?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Filipa Nogueira
Verificado por: Barbara Teixeira

Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por categoria (REQ, DEV, TST,…)
Identificador do Requisito: 2.C

Nome: Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por categoria (REQ, DEV, TST,…)
Descricao: Como utilizador quero que seja apresentada uma lista (tabela) de todos os ficheiros do repositório para assim conseguir ter acesso a tudo devidamente separado e organizado.

Criterios de Aceitacao Funcionais:
- Os ficheiros do repositório tem que estar sempre organizados por categorias (REQ, DEV, TST,…).

Criterios de Aceitacao Nao Funcionais:
- Será possível alterar a lista dos ficheiros?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Joao Silva
Verificado por: Joao Matos (Por fazer)

Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por autor
Identificador do Requisito: 2.D

Nome: Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por autor
Descricao: Como gestor de projeto, pretendo visualizar quais são os ficheiros da autoria de certo utilizador

Criterios de Aceitacao Funcionais:
- Acessar os ficheiros da autoria de certo utilizador
- Selecionar de que utilizador quero visualizar os ficheiros

Criterios de Aceitacao Nao Funcionais:
- Deverá ser possível saber qual a diretoria de cada ficheiro?
- Um utilizador com muitos ficheiros criados pode ter mais problemas (em termos de performance) do que um utilizador que tenha poucos ficheiros criados?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Barbara Teixeira
Verificado por: Filipa Nogueira

Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por intensidade de alterações.
Identificador do Requisito: 2.E

Nome: Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por intensidade de alterações.
Descrição: Como utilizador quero que seja apresentada uma lista (tabela) de todos os ficheiros do repositório organizados por intensidade de alterações de forma a estar tudo organizado e para que possa aceder às alterações feitas nos ficheiros.

Critérios de Aceitação Funcionais:
- A intensidade de alterações deve estar apresentado numa lista de todos os ficheiros do repositório.

Critérios de Aceitação Não Funcionais:
- Como percebemos que os ficheiros foram alterados?
- Nas alterações realizadas é indicado o nome de quem realizou a alteração?
- Como é que é calculada a intensidade?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Luis Goncalves
Verificado por: Beatriz Ferreira

Deverá ser possivel conhecer a história de cada ficheiro.
Identificador do Requisito: 3.A

Nome: Deverá ser possivel conhecer a história de cada ficheiro.
Descrição: Como um membro ativo de um projeto, devo ter acesso a todas as versões de todos os seus ficheiros, para que possa visualizar ou retornar a todas as suas versões.

Critérios de Aceitação Funcionais:
- Deve ser possível ver a mensagem enviada com o commit de cada versão.
- Deve ser apresentada uma linha temporal da história de cada ficheiro, desde a sua criação.
- Cada elemento na timeline deve servir como acesso a cada versão do ficheiro.
- Deve ser possível comparar duas versões de um ficheiro lado a lado.
- Deve ser possível retornar à página do diretório do ficheiro ao qual estamos a aceder.

Criterios de Aceitação Nao Funcionais:
- A visualização da timeline é de fácil acesso quando um ficheiro tem um elevado número de versões?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Miguel Faria
Verificado por: Alexandre van Velze

Saber quais os issues que estão associados a um ficheiro
Identificador do Requisito: 3.B

Nome: Deverá ser possivel saber, por ficheiro, quais os issues que lhe estão associados
Descrição: Como membro de um projeto gitlab, pretendo saber quais os issues que estão associados a cada ficheiro, de maneira a saber que pessoas estão envolvidas no desenvolvimento do ficheiro, assim como as alterações que foram realizadas sobre ele.

Critérios de Aceitação Funcionais:
- Deverá ser possível consultar cada issue (e não só saber quais são)
- Os issues devem estar ordenados por ordem de criação ou data de vencimento (due date)

Critérios de Aceitação Não Funcionais:
- Apresentar a lista completa de ficheiros com todos os seus issues associados causará problemas de performance? (confiabilidade)

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Joao Santos
Verificado por: Isabel Carvalho

Na vista de ficheiros deverá ser possivel visualizar a informação estática sobre o ficheiro.
Identificador do Requisito: 3.C

Nome: A informação estática sobre o ficheiro.
Descrição: Na vista de ficheiros o utilizador deverá conseguir visualizar a informação estática sobre o ficheiro de maneira a distinguir items e avaliá-los.

Critérios de Aceitação Funcionais:
- Visualizar informação estática do ficheiro.
- Visualizar informação como: nome, tipo, tamanho, métricas de estabilidade e, se código, a sua complexidade.

Critérios de Aceitação Não Funcionais:
- Deverá ser informação atual.

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Pedro Martins
Verificado por: Mario Lemos

Deverá existir uma página com a lista de membros ativos do repositório
Identificador do requisito: 4.1.A

Nome: Listagem de todos os membros ativos num repositório.
Descrição: Como utilizador pretendo poder visualizar uma lista contendo todos os membros ativos num determinado repositório.

Critérios de aceitação funcionais:
- Apenas devem ser apresentados os utilizador com, pelo menos, 1 commit efetuado no repositório em questão.
- Os membros devem estar ordenados por ordem alfabética, de forma a facilitar a procura dos mesmos.

Critérios de aceitação não funcionais:
- Poderá um repositório com um número elevado de membros ativos causar problemas ou lentidão no servidor?
- Poderão todos os visitantes obter a lista completa de qualquer repositório, ou haverão algumas exceções?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Mario Lemos
Verificado por: Pedro Martins

Deverá ser possível visualizar as contribuições por categoria (REQ, PM, DEV, TST...) de todos os membros ativos
Identificador do Requisito: 4.1.B

Nome: Deverá ser possível visualizar as contribuições por categoria (REQ, PM, DEV, TST...) de todos os membros ativos.
Descrição: Como utilizador, pretendo poder visualizar todas as contribuições, efetuadas por membros ativos, organizadas por categorias; sendo então mais fácil analisa-las.

Critérios de Aceitação Funcionais:
- Contribuições por terminar devem ser apresentadas?
- Posso visualizar só as categorias que escolher?

Critérios de Aceitação Não Funcionais:
- (Disponibilidade) Sempre que é adicionada uma nova contribuição deve estar disponível para ser apresentada?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Joao Melo
Verificado por: Joao Lopes Monteiro

Deverá ser possível visualizar o esforço total aplicado no projeto, por categoria (REQ, PM, DEV, TST...)
Identificador do Requisito: 4.1.C

Nome: Deverá ser possível visualizar o esforço total aplicado no projeto, por categoria (REQ, PM, DEV, TST...)
Descrição: Como gestor de projeto, quero ver o esforço total dedicado dividido por categoria, de modo a obter uma melhor perspetiva do estado atual do projeto.

Critérios de Aceitação Funcionais:
- O esforço deve estar apresentado por categoria (REQ, PM, DEV, TST...).
- Cada categoria, irá possuir o número de issues, commits, e tempo despendido, definindo assim o esforço respetivo.

Critérios de Aceitação Não Funcionais:
- Usando estas informações como é que o esforço é calculado? (confiabilidade)
- Todos os issues são contabilizados para o esforço, ou apenas os fechados? (confiabilidade)

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Fabio Vaqueiro
Verificado por: Joao Melo

Visualização da evolução do esforço aplicado no projeto, por categoria (REQ, PM, DEV, TST...)
Identificador do Requisito: 4.1.D

Nome: Deverá ser possível visualizar a evolução do esforço aplicado no projeto, por categoria (REQ, PM, DEV, TST...).
Descrição: Como gestor de projeto quero ver a progressão do esforço total aplicado, de modo a verificar todo o empenho e evolução na participação do projeto.

Critérios de Aceitação Funcionais:
- A evolução do esforço deverá ser apresentado por Categoria (REQ, PM, DEV, TST...)
- Por cada categoria deve ser apresentada graficamente a evolução do esforço aplicado, podendo ser vista por quantidade de tempo definida (semana, mês, etc..).

Critérios de Aceitação Não Funcionais:
- O esforço poderá ser definido apenas por issues fechados, ou abertos e fechados? (confiabilidade)

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Francisco Pires
Verificado por: Joao Santos

Deverá ser possivel observar a página de perfil da cada membro activo.
Identificador do Requisito: 4.2.A

Nome: Deverá ser possível observar a página de perfil da cada membro activo.
Descrição: Como gestor do projeto, necessito visualizar a página de perfil de cada membro activo para avaliar o trabalho de cada membro.

Critérios de Aceitação Funcionais:
- Acesso à pagina de perfil de cada membro ativo.

Critérios de Aceitação Não Funcionais:
- Membros ativos podem visualizar o perfil de outro membro ativo?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Alexandre van Velze
Verificado por: Antonio Correia

Devera ser possivel ver a lista de contribuicoes por categoria (REQ, PM, DEV,...) de cada membro ativo.
Identificador do Requisito: 4.2.B

Nome: Devera ser possivel ver a lista de contribuicoes por categoria (REQ, PM, DEV,...) de cada membro ativo.
Descricao: Como gestor de projeto, necessito de visualizar as contribuicoes por categoria de cada membro, com o objetivo de saber em que categoria os membros estao mais ativos e, tambe, se o trabalho esta bem distribuido entre as mesmas.

Criterios de Aceitacao Funcionais:
- Devo conseguir distinguir as diferentes contribuicoes ou apenas quantas foram feitas?
- As contribuicoes devem estar ordenadas por categorias?
- Um ficheiro que foi adicionado e mais tarde removido deve contar como contribuicao?

Criterios de Aceitacao Nao Funcionais:
- (Disponibilidade) A lista deve ser atualizada sempre que um novo commit e feito ou apenas de, por exemplo, hora em hora?
- (Seguranca) Os membros devem ter acesso as contribuicoes de outros?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Joao Matos
Verificado por: 

Deverá ser possível visualizar o papel de cada membro ativo a partir da intensidade das suas contribuições por categoria
Identificador do Requisito: 4.2.C

Nome: Deverá ser possível visualizar o papel de cada membro ativo a partir da intensidade das suas contribuições por categoria. 
Descrição: Como gestor do projeto, pretendo visualizar o papel de cada membro ativo, relativamente ao número de contribuições por categoria.

Critérios de Aceitação Funcionais:
- Acesso às contribuições por categoria de cada membro ativo. A intensidade de cada membro ativo deve estar apresentada por categoria (REQ, PM, DEV, TST, etc).
- Dentro de cada categoria deverá haver uma lista com todos os issues, com os membros que participaram na sua realização.

Critérios de Aceitação Não Funcionais:
- Membros ativos podem visualizar a intensidade de contribuição de outro membro ativo?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Joao Lopes
Verificado por: Miguel Faria

Deverá ser possível observar a lista (tabela) de commits de cada membro ativo.
Identificador do Requisito: 4.2.D

Nome: Deverá ser possível observar a lista (tabela) de commits de cada membro ativo.
Descrição: Como membro do projeto, necessito visualizar todo o conteúdo entregue listado por autor, com o objetivo de saber que contribuição foi feita por cada membro mais facilmente.

Critérios de Aceitação Funcionais:
- Possíbilidade de escolher um membro em específico, onde se deverá mostrar os seus commits
- Commits devem poder ser organizados por data (ascendente ou descendente)

Critérios de Aceitação Não Funcionais:
- Membros que não pertencem à equipa deverão ter acesso ao histórico de commits de membros da equipa? (disponibilidade)
- Commits não poderão ser organizados por outros critérios (por ficheiro, por exemplo)? (organização)

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: David Forte
Verificado por: Fabio Vaqueiro

Visualizar a timeline de issues ao longo do tempo
Identificador do Requisito: 5.A

Nome: Deverá ser possível visualizar a timeline de issues ao longo do tempo: abertos, ativos e total.
Descricao: Como gestor de projeto, pretendo visualizar a timeline de issues abertas e ativas para entender quais questões estão a ser resolvidas e/ou ainda não estão a ser resolvidas.

Criterios de Aceitacao Funcionais:
- A timeline deve conter todos os issues abertos, ativos e o total destes.
- Os issues devem estar bem ordenados consoante apenas a data.

Criterios de Aceitacao Nao Funcionais:
- A timeline pode ser vista por pessoas que não fazem parte da equipe?
- Se os issues não tiverem data, aparecem no inicio ou no fim?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Ana Gil
Verificado por: David Forte

Clicando em cada instante poderemos ver a lista de issues abertos e activos desse momento
Identificador do Requisito: 5.B

Nome: Clicando em cada instante poderemos ver a lista de issues abertos e activos desse momento
Descrição: Como gestor de projeto pretendo que ao clicar num instante poder ver a lista de issues abertos e activos para saber quais issues precisam ser resolvidos ou terminados.

Critérios de Aceitação Funcionais:
- Ao clicar no momento as issues daquele momento aparecem?

Critérios de Aceitação Não Funcionais:
- Aparecem issues fechadas?
- Podemos clicar em momentos futuros?

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Bruno Marques
Verificado por: Ana Gil (Por fazer)

Cada página de visualização deverá ter uma ‘timeline’ que permita ver o estado do repositório em cada instante
Identificador do Requisito: 6.A

Nome: Cada página de visualização deverá ter uma ‘timeline’ que permita ver o estado do repositório em cada instante
Descrição: Como utilizador, necessito de ter uma linha cronológica que permita ver o estado do repositório em cada momento.

Critérios de Aceitação Funcionais:
- atualização em tempo real de cada modificação do repositório (?)

Critérios de Aceitação Não Funcionais:

------------------------------------------------------------------------------------------------------------------------------------------------------------------

Autor: Antonio Correia
Verificado por: Joao Silva

Devera ser possivel ver, por cada commit, quais os ficheiros que foram modificados nesse commit, bem como o respetivo autor.
Identificador do Requisito: 6.C

Nome: Devera ser possivel ver, por cada commit, quais os ficheiros que foram modificados nesse commit, bem como o respetivo autor.
Descricao: Como membro ativo do projeto, devo ter acesso a uma lista de ficheiros, os quais foram modificados num commit previamente selecionado, sendo identificado o autor do mesmo, de forma a identificar mais facilmente um commit que alterou o ficheiro que me interessa.

Criterios de Aceitacao Funcionais:
- Devo conseguir ver o numero de ficheiros alterados por commit?
- Devo conseguir ver um commit que foi mais tarde cancelado?

Criterios de Aceitacao Nao Funcionais:
- (Disponibilidade) A lista deve ser atualizada sempre que um novo commit e feito ou apenas de, por exemplo, hora em hora?
- (Seguranca) Devo conseguir ver todos os commits ou apenas aqueles permitidos?
- (Seguranca) Devo ter acesso ao conteudo de cada ficheiro ou apenas ao nome do mesmo?

------------------------------------------------------------------------------------------------------------------------------------------------------------------