StormLab![](Aspose.Words.ba7cadbc-8fe5-404f-9246-eaecdb3e118c.001.png)

by Softwork 

Universidade De Coimbra | FCTUC 

Engenharia de Software

**1\_Equipa SoftWork**

1. Projeto a ser testado: StormLab 
1. Email de contacto:[ lopix23@gmail.com ](mailto:lopix23@gmail.com)


**2\_ID da versão com data** 

Versão 2.0 (17/12/2021)


**3\_Link para a versão online** 

(https://stormlab.herokuapp.com/) e 10.17.0.174:8080 (Tem de estar ligado à rede do DEI).


**4\_Link para a lista de requisitos** 

https://gitlab.com/espl3/es-pl3/-/blob/main/README.md 


**5\_Lista de requisitos (por extenso) suportado pelo website** 

\_Identificador do Requisito: 1.A 

Escolha do repositório GitLab do utilizador a analisar. 

\_Identificador do Requisito: 4.1.A 

Criação de uma página com a lista9 de membros ativos (efetuou pelo menos 1 commit) do repositório.

\_Identificador do Requisito:4.1.B 

Visualização das contribuições por categoria1 9 de todos os membros ativos.

\_Identificador do Requisito: 4.1.C 

Esforço total aplicado no projeto, por categoria1 9.

\_Identificador do Requisito:4.2.B 

Visualização da lista de contribuições por categoria1 8.

\_Identificador do Requisito: 4.2.D 

Observação da lista (tabela) de commits de cada membro ativo do repositório4.

\_Identificador do Requisito:4.2.E 

Visualização da timeline de commits de cada membro ativo3.

\_Identificador do Requisito: 5.A 

Visualização da timeline de issues7 ao longo do tempo (abertos, ativos e total).

\_Identificador do Requisito: 6.C 

Visualização, por commit, dos ficheiros alterados, assim como o respetivo autor2 6.

\_Identificador do Requisito: N/A 

Realização do login com o token do GitLab do utilizador5. 


Novidades do novo deploy: 

\_Identificador do Requisito: 1.B 

Escolha do branch a ser analisado. 

\_Identificador do Requisito: 1.F 

Funcionalidade “back” para voltar à página anterior. 

\_Identificador do Requisito: 1.G 

Novos acessos à dashboard envolve a recolha dos dados de atividade. 

\_Identificador do Requisito 1.H

Visualização da última atualização no ecrã.

\_Identificador do Requisito: 2.A 

Apresentação da árvore de ficheiros do repositório10. 

\_Identificador do Requisito: 2.B 

Visualização dos ficheiros presentes dentro de cada diretoria/pasta10. 

\_Identificador do Requisito: 2.C 

Observação da lista de ficheiro do repositório por categoria1, 11 . 

\_Identificador do Requisito: 3.A.1 

Apresentação da lista de commits que atualizou um ficheiro (e quem). 

\_Identificador do Requisito: 3.A.2 

Visualização da dimensão das alterações de um ficheiro em cada commit. 

\_Identificador do Requisito: 3.C 

Apresentação da informação estática associada a cada ficheiro. 



1  Por categorias entende-se os parâmetros: TODOS, ARCH, DES, DEV, ENV, PM, PROC, PROD, QA, REC, TST 

2  A visualização desta página encontra-se elaborada com código HTML básico.

3  Acessíveis através do url: https://stormlab.herokuapp.com/members/user/{username}/ {branch}/timeline/

4  Acessíveis através do url: https://stormlab.herokuapp.com/members/user/{username}/ {branch}/table/

5  Deve-se ir buscar o token pessoal ao GitLab. Para isso deve clicar em Preferences>Access Token dar um nome ao mesmo e selecionar a scope “api”. 

6  Acessíveis através do url: https://stormlab.herokuapp.com/commits/{page\_number} 

7  Acessíveis através do url: https://stormlab.herokuapp.com/issues/{open/closed} 

8  Acessíveis através do url: https://stormlab.herokuapp.com/commits/{category} 

9  Acessíveis através do url:[ https://stormlab.herokuapp.com/members ](https://stormlab.herokuapp.com/members)

10  Acessíveis através do url: https://stormlab.herokuapp.com/file 

11  Acessíveis através do url: https://stormlab.herokuapp.com/files/{branch} 



**Membros ativos da equipa SoftWork** 

- Alexandre da Silva van Velze 2019216618
- Ana Gil Oliveira Ferreira 2018302843
- Ana Isabel da Cunha Carvalho 2019213446 
- António Pedro Correia 2019216646
- Bárbara da Palma Teixeira 2019212062
- Beatriz dos Reis Ferreira 2019226233
- Bruno Miguel Santos Marques 2018278019
- David Forte da Ressurreição 2019219749
- Dumitru Brailean 2019226123
- Fábio Miguel Rodrigues Vaqueiro 2019222451
- Filipa Mendes Nogueira 2019212940
- Francisco João Romão Pires 2019201225
- João António da Silva Melo 2019216747
- João Carlos Borges Silva 2019216753
- João Lopes Teixeira Monteiro 2019216764
- João Maria Veloso Cristóvão Santos 2019210135
- Luís Alexandre Gomes Gonçalves 2019228021
- Mário Guilherme de Almeida Martins Sequeira Lemos 2019216792 
- Miguel António Gabriel de Almeida Faria 2019216809
- Nathalie Andreina Pinto Jennings 2019226002 
- Pedro Afonso Ferreira Lopes Martins 2019216826
- Pedro Miguel Cardoso Letra 2019210510
- Susana Pires Batista 2019208639 
