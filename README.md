# **Stormlab**

O StormLab é uma aplicação web desenvolvida pela SoftWork no ambito da cadeira de ES2021 que visa a implementação de um dashboard para gerir repositórios de projectos alojados no GitLab.

Tecnologias usadas:
<p align="left"> </a> <a href="https://www.python.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> <a href="https://www.djangoproject.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/django/django-original.svg" alt="django" width="40" height="40"/> </a> <a href="https://heroku.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/heroku/heroku-icon.svg" alt="heroku" width="40" height="40"/> </a> <a href="https://gitlab.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/gitlab/gitlab-icon.svg" alt="gitlab" width="40" height="40"/> </a> </p>

## **Índice**
* [**Guia de Instalação**](https://gitlab.com/espl3/es-pl3/-/blob/main/README.md)
    * [Instalação do Git - Parte 1](https://gitlab.com/espl3/es-pl3/-/blob/main/README.md)
    * [Instalação do Git - Parte 2](https://gitlab.com/espl3/es-pl3/-/blob/main/README.md)
    * [Correr o Website](https://gitlab.com/espl3/es-pl3/-/blob/main/README.md)
* [**Requisitos Implementados**](https://gitlab.com/espl3/es-pl3/-/blob/main/README.md)  
* [**Testes**](https://gitlab.com/espl3/es-pl3/-/blob/main/README.md)    
* [**Documentação**](https://gitlab.com/espl3/es-pl3/-/blob/main/README.md)


## **Guia de Instalação**

### Instalação do Git - Parte 1
O git pode ser instalado nos sistemas operativos mais comuns atualmente, tais como o Windows, Mac e Linux. Normalmente o Git já vem instalado por default na maior parte dos computadores Mac e Linux.
Para verificar se o Git já está instalado, é necessário utilizar a consola.
* No Mac, abrir o "Terminal".
* No Windows, abrir a linha de comandos (windows command prompt).

Escreva o seguinte comando:
```
git version
```
O output irá indicar a versão se estiver instalado, ou, no caso de não estar, _comando desconhecido (unknown command)_.

### Instalação do Git - Parte 2

**Instalar Git no Windows**

1.  Ir ao [site oficial](https://gitforwindows.org/) e fazer download da versão mais recente.
2.  Ao correr o instalador, seguir as instruções até o processo de instalação estar concluido.
3.  Verificar que o Git foi instalado ao abrir o terminal e introduzir o comando `git version`.

**Instalar Git no Mac**

*   A partir do instalador:
    1.  Ir ao site com o [Instalador para macOS](https://sourceforge.net/projects/git-osx-installer/files/) e fazer download da versão mais recente.
    2.  Ao correr o instalador, seguir as instruções até o processo de instalação estar concluido.
    3.  Verificar que o Git foi instalado ao abrir o terminal e introduzir o comando `git version`.

* A partir do Homebrew: 
    1.  Abrir uma janela do terminal e instalar o Git através do comando:
    ```
    brew install git
    ```
    2. Verificar que o Git foi instalado ao introduzir o comando `git version`.

**Instalar Git no Linux**

1. Abrir o terminal e inserir o seguinte comando para atualizar os packages:
```
sudo apt-get update
```
2. Para instalar o Git, insira o comando:
```
sudo apt-get install git-all
```
3. Verificar que o Git foi instalado ao introduzir o comando `git version`.

### Correr o Website

1. Para ligar o site localmente, insira o comando:
```
python manage.py runserver
```

## **Requisitos Implementados**

1.
    - [x] A
    - [x] B
    - [ ] C
    - [ ] D
    - [ ] E
    - [x] F
    - [x] G
    - [x] H
<br ><br />
2.
    - [x] A
    - [x] B
    - [x] C
    - [ ] D
    - [ ] E
<br ><br />
3.
    - [ ] A
        - [x] A.1
        - [x] A.2
        - [ ] A.3
    - [ ] B
    - [x] C
<br ><br />
4.
    - [x] 1.A
    - [x] 1.B
    - [x] 1.C
    - [ ] 1.D
    - [ ] 2.A
    - [x] 2.B
    - [ ] 2.C
    - [x] 2.D
    - [x] 2.E
<br ><br />
5.
    - [x] A
    - [ ] B
<br ><br />
6.
    - [ ] A
    - [ ] B
    - [x] C

Para mais detalhes, ver a lista de requisitos e os [issues abertos](https://gitlab.com/espl3/es-pl3/-/issues?scope=all&state=opened).

## **Testes**

## **Documentação**

### Geral

- [Atas](https://gitlab.com/espl3/es-pl3/-/tree/main/PM/minutes)
- [Lista de Cargos](https://gitlab.com/espl3/es-pl3/-/blob/main/PM/profiles/README.md)
- [Layouts](https://www.figma.com/file/oHXbTPoTZXmKuyODgC4CMZ/Layouts?node-id=0%3A1)

### Manuais

### Arquitetura

### Requisitos
- Lista de Requisitos
