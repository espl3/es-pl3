Desenho do produto 4.1.A
Identificador do Requisito: 4.1.A

Nome: Deverá existir uma página com a lista de membros ativos do repositório. Um membro ativo é aquele que fez pelo menos um commit.

Etapas:

Utilizador entra na página inicial.
Utilizador seleciona um projeto.
Utilizador dirige-se à aba de "Membros".
Sistema devolve a lista de membros ordenados por ordem alfabética e com um ou mais commits efetuados.
Finalidade: Listar todos os membros "ativos" de um repositório GitLab.