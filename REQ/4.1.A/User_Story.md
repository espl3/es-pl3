Identificador do requisito: 4.1.A
Nome: Listagem de todos os membros ativos num repositório.
Descrição: Como utilizador pretendo poder visualizar uma lista contendo todos os membros ativos num determinado repositório.
Critérios de aceitação funcionais:

Apenas devem ser apresentados os utilizador com, pelo menos, 1 commit efetuado no repositório em questão.
Os membros devem estar ordenados por ordem alfabética, de forma a facilitar a procura dos mesmos.

Critérios de aceitação não funcionais:

Um repositório com um número elevado de membros ativos pode causar problemas ou lentidão no servidor
Poderão todos os visitantes obter a lista completa de qualquer repositório, ou haverão algumas exceções