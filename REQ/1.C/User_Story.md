Identificador do Requisito: 1.C
Nome: A visualização temporal (linha de tempo) deverá ser uniforme, i.e. as unidades de tempo devem corresponder ao mesmo comprimento na sua visualização.
Descrição: Como membro ativo de um projeto, devo ver os intervalos temporais nas linhas de tempo de forma uniforme, para facilitar a navegação e interpretação da timeline.
Critérios de Aceitação Funcionais:

A timeline não deve ocupar somente a largura da janela do browser. Deve ocupar uma janela navegável na horizontal, para que a informação tenha o espaço adequado à sua leitura.
A timeline deverá estar dividida em segmentos iguais de tempo. Cada um dos segmentos deve corresponder por default a 1 semana de um mês. Cada uma dessas semanas deve estar dividida pelos seus dias.

Criterios de Aceitação Nao Funcionais:

A timeline deve estar sempre atualizada.  [(?)a sua visualização começa momento do acesso, ou numa unidade inteira de tempo?]
A timeline deve ser capaz de apresentar um projeto inteiro independentemente da sua duração.