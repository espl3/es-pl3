Identificador do Requisito: 2.A
Nome: Deverá ser apresentada a árvore de ficheiros do repositório
Descricao: Como membro ativo de um projeto, devo poder visualizar e navegar pela árvore de ficheiros do repositório, de forma a que possa facilmente aceder ao conteúdo dos ficheiros.
Criterios de Aceitacao Funcionais:

Por todos os ficheiros e pastas presentes no repositório, deve ser possível ver o seu nome e extensão.
Deve ser possível aceder a todos os ficheiros e pastas na árvore de ficheiros.
Deve ser possível alterar o branch ao qual estamos a aceder.
Deve ser possível fazer uma procura relativa aos ficheiros e pastas presentes no repositório.
Ao navegar pelas pastas deve ser possível retornar à pasta anterior.
A mensagem e a data do último commit deve ser apresentado.

Critérios de Aceitacao Nao Funcionais:

A árvore de ficheiros deve atualizar sempre que for feita uma alteração ao repositório.