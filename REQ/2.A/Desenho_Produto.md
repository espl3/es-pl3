Desenho do produto 2.A
Identificador do Requisito: 2.A

Nome:Deverá ser apresentada a árvore de ficheiros do repositório

Etapas:

Árvore de ficheiros organizada por pastas e subpastas
Utilizador visualiza a mensagem e data do último commit
Utilizador tem acesso a todos os ficheiros da árvore do repositório
Finalidade: Visualizar a árvore de ficheiros do Repositório e navegar pelos respetivos ficheiros e pastas.