Identificador do Requisito:1.H
Nome:  O momento da última atualização dos dados deverá estar sempre presente no écran (e.g. “Última atualização há 3 minutos”).
Descrição: Como membro ativo, necessito visualizar o momento da última atualização dos dados para ter certeza de que tenho a versão atual dos dados.
Critérios de Aceitação Funcionais:

Unidade de tempo definida (minutos, horas, dias,...)

Critérios de Aceitação Não Funcionais:

A visualização da última atualização estará sempre presente no écran em todas as webpages do website.
Será disponibilizado o tempo decorrido (em minutos, horas, dias) da última atualização

Tempo despendido (em pommodoros): 2