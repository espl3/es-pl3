Identificador do Requisito: 1.E
Nome:Os eventos na linha temporal devem estar organizado por commit instants.
Descrição:Como utilizador ativo do projeto devo estruturar os eventos na linha temporal com a devida utilização de commit instants.
Critérios de Aceitação Funcionais:

Deve poder ter acesso a todos eventos da linha temporal dos mais recentes e dos mais antigos.
Deve ser usado obrigatoriamente commit instantes para estruturas os eventos.

Critérios de Aceitação Não Funcionais:

pessoas fora da equipa conseguem aceder a esta linha temporal e ver os eventos ocorridos?