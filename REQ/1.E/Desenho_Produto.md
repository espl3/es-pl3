Identificador do Requisito: 1.E
Nome:Os eventos na linha temporal devem estar organizado por commit instants.
Etapas:

o utilizador entra na página inicial
acede ao repositório desejado
utilizador visualiza os eventos na linha temporal organizados por commits.

Finalidade: Visualizar os eventos organizados por commit instants na linha temporal.