Identificador do Requisito: 3.C
Nome: Na vista de ficheiros deverá ser possivel visualizar a informação estática sobre o ficheiro.
Etapas:

Utilizador encontra-se na vista de ficheiros
Sistema mostra lista de ficheiros com as cracterísticas: nome e tipo
Utilizador seleciona ficheiro
Sistema mostra ficheiro selecionado com todas as características: nome, tipo, tamanho, métricas de estabilidade e, se código, a sua complexidade.

Finalidade: Ser possivel visualizar a informação estática sobre o ficheiro