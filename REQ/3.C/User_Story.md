Identificador do Requisito: 3.C
Nome: A informação estática sobre o ficheiro.
Descrição: Na vista de ficheiros o utilizador deverá conseguir visualizar a informação estática sobre o ficheiro de maneira a distinguir items e avaliá-los.
Critérios de Aceitação Funcionais:

Visualizar informação estática do ficheiro.
Visualizar informação como: nome, tipo, tamanho, métricas de estabilidade e, se código, a sua complexidade.

Critérios de Aceitação Não Funcionais:

Deverá ser informação atual.