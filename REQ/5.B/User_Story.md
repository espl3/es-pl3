Identificador do Requisito: 5.B
Nome: Clicando em cada instante poderemos ver a lista de issues abertos e activos desse momento
Descrição: Como gestor de projeto pretendo que ao clicar num instante poder ver a lista de issues abertos e activos para saber quais issues precisam ser resolvidos ou terminados.
Critérios de Aceitação Funcionais:

Devem aparecer apenas os issues daquele momento.

Critérios de Aceitação Não Funcionais:

Não devem aparecer issues já fechadas.