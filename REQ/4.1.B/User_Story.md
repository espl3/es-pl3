Identificador do Requisito: 4.1.B
Nome: Deverá ser possível visualizar as contribuições por categoria (REQ, PM, DEV, TST...) de todos os membros ativos.
Descrição: Como utilizador, pretendo poder visualizar todas as contribuições, efetuadas por membros ativos, organizadas por categorias; sendo então mais fácil analisa-las.
Critérios de Aceitação Funcionais:

Contribuições por terminar devem ser apresentadas, aparecendo primeiro as concluídas.
Devo poder visualizar só as categorias que escolher ou, se não escolher nenhuma, visualizar todas.

Critérios de Aceitação Não Funcionais:

(Disponibilidade) Sempre que é adicionada uma nova contribuição deve estar disponível para ser apresentada.