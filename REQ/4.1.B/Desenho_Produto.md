Identificador do Requisito:4.1.B
Nome:Deverá ser possível visualizar as contribuições por categoria (REQ, PM, DEV, TST...) de todos os membros ativos.
Etapas:

Utilizador entra na página inicial.
Utilizador seleciona um projeto.
Utilizador seleciona a categoria pretendida.
Contribuições serão apresentadas.

Finalidade:Visualização das contribuições de todos os membros ativos(por categoria).