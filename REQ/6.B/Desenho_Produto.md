Identificador do Requisito: 6.B
Nome: Deverá ser possível visualizar a evolução da árvore de ficheiros ao longo do tempo. Isto é, manipulando a timeline (cada segmento é um intervalo entre dois commits).
Etapas:

Utilizador entra na pagina inicial
Seleciona Repositório desejado
Seleciona a timeline que deseja para visualizar a evolução

Finalidade: Ser possível visualizar a evolução da árvore de ficheiros ao longo do tempo, escolhendo uma timeline.