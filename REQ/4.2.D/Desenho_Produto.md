Identificador do Requisito: 4.2.D
Nome: Deverá ser possível observar a lista (tabela) de commits de cada membro ativo.
Etapas:

Membro ativo entra na pagina inicial
Seleciona Repositório desejado
Seleciona Membros
Selecionamento do membro em questão
Obter commits filtrados por membro
Sort dos commits por data após serem filtrados

Finalidade: Obter todos os commits por membro, ordenados por data