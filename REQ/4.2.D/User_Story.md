Identificador do Requisito: 4.2.D
Nome: Deverá ser possível observar a lista (tabela) de commits de cada membro ativo.
Descrição: Como membro do projeto, necessito visualizar todos os commits listados por autor, com o objetivo de saber que contribuição foi feita por cada membro mais facilmente.
Critérios de Aceitação Funcionais:

Possíbilidade de escolher um membro em específico, onde se deverá mostrar os seus commits
Commits devem poder ser organizados por data

Critérios de Aceitação Não Funcionais:

Membros que não pertencem à equipa não deverão ter acesso ao histórico de commits de membros da equipa. (disponibilidade)
(atualização: o token utilizado na autenticação apenas permite pessoas que sejam membros do projeto aceder ao mesmo garantindo que este critério é sempre cumprido, deste modo não é necessário condições que permitam ou não a visualização do perfil)