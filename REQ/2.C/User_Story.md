Identificador do Requisito: 2.C
Nome: Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por categoria (REQ, DEV, TST,…)
Descricao: Como utilizador quero que seja apresentada uma lista (tabela) de todos os ficheiros do repositório para assim conseguir ter acesso a tudo devidamente separado e organizado.
Criterios de Aceitacao Funcionais:

Os ficheiros do repositório tem que estar sempre organizados por categorias (REQ, DEV, TST,…).

Criterios de Aceitacao Nao Funcionais: