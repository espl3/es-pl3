Identificador do Requisito: 2.C
Nome: Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por categoria (REQ, DEV, TST,…)
Etapas:

Utilizador entra na página inicial;
Acede ao repositório desejado;
Seleciona a página dos ficheiros;
Utilizador visualiza a lista de todos os ficheiros agrupados por categoria.

Finalidade: Obter todos os ficheiros agrupados por categorias.