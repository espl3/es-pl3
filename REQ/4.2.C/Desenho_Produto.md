Identificador do Requisito: 4.2.C
Nome: Deverá ser possível visualizar o papel de cada membro ativo a partir da intensidade das suas contribuições por categoria.
Etapas:

Utilizador escolhe o projeto pretendido
Seleciona a opção 'Membros'
Seleciona o membro em questão
Obter commits filtrados por categoria

Finalidade: Obter o papel de cada membro ativo a partir da sua intensidade de contribuições por categoria.