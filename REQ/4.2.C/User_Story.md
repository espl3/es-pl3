Identificador do Requisito: 4.2.C
Nome: Deverá ser possível visualizar o papel de cada membro ativo a partir da intensidade das suas contribuições por categoria.
Descrição: Como gestor do projeto, pretendo visualizar o papel de cada membro ativo, relativamente ao número de contribuições por categoria.
Critérios de Aceitação Funcionais:

Acesso às contribuições por categoria de cada membro ativo. A intensidade de cada membro ativo deve estar apresentada por categoria (REQ, PM, DEV, TST, etc).
Dentro de cada categoria deverá haver uma lista com todos os issues, com os membros que participaram na sua realização.



Critérios de Aceitação Não Funcionais:

Membros ativos podem visualizar a intensidade de contribuição de outro membro ativo?
