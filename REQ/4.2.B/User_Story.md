Identificador do Requisito: 4.2.B
Nome: Deverá ser possível ver a lista de contribuições por categoria (REQ, PM, DEV,...) de cada membro ativo.
Descrição: Como gestor de projeto, necessito de visualizar as contribuições por categoria de cada membro, com o objetivo de saber em que categoria os membros estão mais ativos e também se o trabalho está bem distribuído entre as mesmas.
Critérios de Aceitação Funcionais:

Um ficheiro que foi adicionado e mais tarde removido não deve contar como contribuição.

Critérios de Aceitação Não Funcionais:

(Disponibilidade) A lista deve ser atualizada sempre que for requisitada.
(Segurança) Os membros devem ter acesso as contribuições de outros.