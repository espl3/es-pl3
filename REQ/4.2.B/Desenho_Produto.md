Identificador do Requisito: 4.2.B
Nome: Deverá ser possível ver a lista de contribuições por categoria (REQ, PM, DEV,...) de cada membro ativo.
Etapas:

Membro ativo entra na pagina inicial
Seleciona Repositório desejado
Seleciona Membros
Selecionamento do membro em questão
Obter os commits desse membro
Ordenar os commits por categoria (REQ, PM, DEV,...)

Finalidade: Obter todas as contribuições de um membro ativo ordenadas por categoria.