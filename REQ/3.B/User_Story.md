Identificador do Requisito: 3.B
Nome: Deverá ser possivel saber, por ficheiro, quais os issues que lhe estão associados
Descrição: Como membro de um projeto gitlab, pretendo saber quais os issues que estão associados a cada ficheiro, de maneira a saber que pessoas estão envolvidas no desenvolvimento do ficheiro, assim como as alterações que foram realizadas sobre ele.
Critérios de Aceitação Funcionais:

Deverá ser possível consultar cada issue (e não só saber quais são)
Os issues devem estar ordenados por ordem de criação ou data de vencimento (due date)

Critérios de Aceitação Não Funcionais:

(Confiabilidade) Verificar se a apresentação da lista completa de ficheiros com todos os seus issues associados causa problemas de performance (uma vez que é um requisito avançado, não foram apresentados tempos limite por parte do cliente)