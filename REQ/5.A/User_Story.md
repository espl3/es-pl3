Identificador do Requisito: 5.A

Nome: Deverá ser possível visualizar a timeline de issues ao longo do tempo: abertos, ativos e total.

Descricao: Como gestor de projeto, pretendo visualizar a timeline de issues abertas e fechados para entender quais questões estão a ser resolvidas e/ou ainda não estão a ser resolvidas.

Criterios de Aceitacao Funcionais:

A timeline deve conter todos os issues abertos, fechados e o total destes.
Os issues devem estar bem ordenados consoante apenas a data em que foram criados.
Criterios de Aceitacao Nao Funcionais:

A timeline pode ser vista por pessoas que não fazem parte da equipe?
Se os issues não tiverem data, aparecem no inicio ou no fim?