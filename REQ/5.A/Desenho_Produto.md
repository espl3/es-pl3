Identificador do Requisito: 5.A
Nome: Deverá ser possível visualizar a timeline de issues ao longo do tempo: abertos, fechados e total.
Etapas:

Utilizador seleciona o seu projeto e acede à página inicial
Seleciona a opção Issues
Seleciona o tipo de Issues que pretende ver, se aberto, fechados ou total
Visualiza os issues ordenados por data em que foram criados
Seleciona o Issue

Finalidade: Observar uma lista de todos os issues abertos ou fechados ou o total destes.