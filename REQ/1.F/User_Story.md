Identificador do Requisito: 1.F
Nome: Deverá existir funcionalidade ‘back’, i.e. regressar à vista anteriormente apresentada sem ter de reinserir as opções que levaram à sua visualização.
Descricao: Como utilizador quero ter uma funcionalidade ‘back’ para ter facilidade no acesso ao site.
Criterios de Aceitacao Funcionais:

Terá de haver um botão que volte à página imediatamente anterior.
Guardar as opções inseridas.

Criterios de Aceitacao Nao Funcionais:

As opções são guardadas para uma próxima utilização a longo prazo.