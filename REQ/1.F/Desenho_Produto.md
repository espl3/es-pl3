Identificador do Requisito: 1.F
Nome: Deverá existir funcionalidade ‘back’, i.e. regressar à vista anteriormente apresentada sem ter de reinserir as opções que levaram à sua visualização.
Etapas:

Utilizador seleciona o seu projeto e acede à página inicial;
Ao clicar no botão 'back' (presente em todas as páginas) volta à página anterior;
Deverá ser apresentada a página com as configurações (filtros de pesquisa, etc.) previamente inseridas.

Finalidade: Tornar o site mais funcional.