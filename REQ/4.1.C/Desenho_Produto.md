Identificador do Requisito:4.1.C
Nome: Deverá ser possível visualizar o esforço total aplicado no projeto, por categoria (REQ, PM, DEV, TST...)
Etapas:

Utilizador escolhe o projeto pretendido
Seleciona a opção 'Membros'
Obtém o esforço total
Poderá selecionar uma categoria em específico

Finalidade: Visualizar o esforço total aplicado no projeto por categoria.