Identificador do Requisito: 4.1.C
Nome: Deverá ser possível visualizar o esforço total aplicado no projeto, por categoria (REQ, PM, DEV, TST...)
Descrição: Como membro do projeto, quero ver o esforço total dedicado dividido por categoria, de modo a obter uma melhor perspetiva do estado atual do projeto.
Critérios de Aceitação Funcionais:

O esforço deve estar apresentado por categoria (REQ, PM, DEV, TST...).
Cada categoria, irá possuir o número de linhas alteradas mostrando assim o esforço total dedicado.

Critérios de Aceitação Não Funcionais: