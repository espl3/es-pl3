Identificador do Requisito: 2.B
Nome: Os ficheiros presentes em cada diretoria/pasta deverão poder ser visíveis (nome e extensão dos ficheiros)
Etapas:

Utilizador seleciona o seu projeto e acede à página inicial
Seleciona a opção 'Ficheiros'
Na opção 'Ficheiros' são exibidas todas as pastas existentes no repositório
Seleciona a pasta que pretende aceder e têm acesso aos ficheiros com os respetivos nomes e extensões

Finalidade: Visualizar nome e extensão dos ficheiros presentes em cada pasta/diretoria