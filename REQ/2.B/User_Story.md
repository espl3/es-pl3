Identificador do Requisito: 2.B
Nome: Os ficheiros presentes em cada diretoria/pasta deverão poder ser visíveis (nome e extensão dos ficheiros)
Descrição: Como utilizador quero que o nome e extensão dos ficheiros sejam sempre visíveis para tornar o acesso aos ficheiros mais fácil e rápido.
Critérios de Aceitação Funcionais:

Nome e extensão dos ficheiros sempre visíveis;
O nome dos ficheiros deve ser curto e direto;

Critérios de Aceitação Não Funcionais:

Deve ser possível alterar a ordem do nome dos ficheiros
Deve ser possível ordenar alfabeticamente os ficheiros
Deve ser possível agrupar os ficheiros por extensões