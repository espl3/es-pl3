Identificador do Requisito: 3.A [3.A.1 e 3.A.2]
Nome: Deverá ser possivel conhecer a história de cada ficheiro.
Descrição: Como um membro ativo de um projeto, devo ter acesso a todas as versões de todos os seus ficheiros, para que possa visualizar ou retornar a todas as suas versões.
Critérios de Aceitação Funcionais:

Deve ser possível ver a mensagem enviada com o commit de cada versão.
Deve ser apresentada uma linha temporal da história de cada ficheiro, desde a sua criação.
Cada elemento na timeline deve servir como acesso a cada versão do ficheiro.
Deve ser possível comparar duas versões de um ficheiro lado a lado.
Deve ser possível retornar à página do diretório do ficheiro ao qual estamos a aceder.

Criterios de Aceitação Nao Funcionais:

A visualização da timeline deverá ser de fácil acesso quando um ficheiro tem um elevado número de versões.