Identificador do Requisito: 3.A.1
Nome: Deverá ser possivel conhecer a história de cada ficheiro: A lista de commits que o alterou (e quem) em cada commit.
Etapas:

Na vista dos ficheiros, o utilizador seleciona o ficheiro que pretende abrir.
É redirecionado para a página individual do ficheiro.
O sistema recolhe todos os commits e quem os fez.
Essa informação é disposta numa lista, organizada por dias, na página do ficheiro.

Finalidade: Visualizar os commits de um ficheiro em específico e quem os fez.