Identificador do Requisito: 4.2.A
Nome: Deverá ser possível observar a página de perfil da cada membro activo.
Descrição: Como gestor do projeto, necessito visualizar a página de perfil de cada membro activo para avaliar o trabalho de cada membro.
Critérios de Aceitação Funcionais:

Acesso à pagina de perfil de cada membro ativo.

Critérios de Aceitação Não Funcionais:

(Segurança) Os membros devem ter acesso à pagina de perfil de cada membro ativo.