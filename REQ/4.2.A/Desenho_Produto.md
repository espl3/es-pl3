Identificador do Requisito: 4.2.A
Nome: Deverá ser possível observar a página de perfil da cada membro ativo.
Etapas:

Utilizador entra na pagina inicial
Seleciona Repositório desejado
Seleciona Membros
Seleciona o membro que deseja observar

Finalidade: Ter acesso à pagina de perfil de cada membro ativo.