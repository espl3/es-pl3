Identificador do Requisito: 1.B
Nome: Deverá ser possível escolher o branch que está ser visualizado. Por defeito será o main branch (a visualização por branch será um requisito avançado).
Descrição: Como developer, preciso ter a opção de escolher o branch a ser visualizado para isolar o desenvolvimento sem afetar outras ramificações no repositório
Critérios de Aceitação Funcionais:

O nome do branch a ser visualizado deverá estar sempre presente no écran

Critérios de Aceitação Não Funcionais:

É possível comparar dois branch lado a lado?
É possível trocar as preferencias da visualização do branch por defeito(main)?