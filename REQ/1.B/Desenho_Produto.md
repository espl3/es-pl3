Identificador do Requisito: 1.B
Nome: Deverá ser possível escolher o branch que está ser visualizado. Por defeito será o main branch (a visualização por branch será um requisito avançado).
Etapas:

O utilizador seleciona o seu projeto e acede à página inicial;
Acede ao repositório;
Seleciona a opção "Branches";
Na opção "Branches" são exibidos todos os branches existentes no repositório;
Seleciona o branch que quer visualizar.

Finalidade: Visualizar o branch pretendido no repositório.