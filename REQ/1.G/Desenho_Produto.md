Identificador do Requisito: 1.G
Nome: Deverá envolver a recolha dos dados de atividade mais recente no repositório em cada novo acesso à dashboard.
Etapas:

Utilizador acede à dashboard.
Sempre que é feito novo acesso à dashboard apresenta os últimos 10 issues e os últimos 10 commits recolhidos das atividades mais recentes no repositório (ordenado cronologicamente).
Contém a data mais recente e nomes dos respetivos dados de atividade.
Função para voltar à página anterior.

Finalidade: Visualizar a dashboard com todos dados de atividade mais recente no repositório.