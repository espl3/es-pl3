Identificador do Requisito: 1.G
Nome: Deverá envolver a recolha dos dados de atividade mais recente no repositório em cada novo acesso à dashboard.
Descrição: Como developer, preciso ter uma função para o novo acesso à dashboard, para que possa envolver a recolha dos dados de atividade mais recente no repositório.
Critérios de Aceitação Funcionais:

Implementar a recolha de dados mais recente no repositório.

Critérios de Aceitação Não Funcionais:

A recolha de dados deve conter alguma ordem?
Qualquer membro pode aceder à dashboard?