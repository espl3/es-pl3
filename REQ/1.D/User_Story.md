Identificador do Requisito: 1.D
Nome: A visualização de eventos temporais deverá suportar ampliação/redução (zoom-in/zoom-out)
Descrição: Como membro ativo de um projeto no GitLab, devo conseguir fazer zoom em eventos temporais, de forma a ter uma visualização mais ou menos detalhada deles e a facilitar a procura de momentos mais específicos desses eventos. (requisito avançado e de menor prioridade)
Critérios de Aceitação Funcionais:

Funcionalidade suportada através de botões no ecrã (zoom-in/zoom-out) e de um slider.
Zoom atual deverá ser demonstrado através de um slider.
Modo de zoom default definido no requisito 1.C.
Timeline deverá sempre ser contida dentro da da janela navegável horizontal definida em 1.C.
Zoom-in irá agrupar conjuntos de dias por semana ou mês.
Zoom-out irá separar os dias em secções iguais até um máximo de 4: (1,2,4). Ex: 1 secção de 24h, 4 secções de 6h.

Critérios de Aceitação Não Funcionais:

Não existir pixelização dos objetos gráficos.
Objetos continuarem proporcionais entre si.