Identificador do Requisito: 4.1.D
Nome: Deverá ser possível  visualizar a evolução de esforço aplicado no projeto, por categoria (REQ, PM, DEV, TST...).
Etapas:

Utilizador escolhe o projeto pretendido
Seleciona a opção 'Membros'
Obtém a evolução do esforço total graficamente
Poderá selecionar uma categoria em específico
Poderá selecionar a quantidade de tempo definida

Finalidade: Visualizar a evolução do esforço total aplicado no projeto por categoria.