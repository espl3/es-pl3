Identificador do Requisito: 4.1.D
Nome: Deverá ser possível  visualizar a evolução do esforço aplicado no projeto, por categoria (REQ, PM, DEV, TST...).
Descrição: Como gestor de projeto quero ver a progressão do esforço total aplicado, de modo a verificar todo o empenho e evolução na participação do projeto.
Critérios de Aceitação Funcionais:

A evolução do esforço deverá ser apresentado por Categoria (REQ, PM, DEV, TST...)
Por cada categoria deve ser apresentada graficamente a evolução do esforço aplicado, podendo ser vista por quantidade de tempo definida (semana, mês, etc..).

Critérios de Aceitação Não Funcionais:

O esforço poderá ser definido apenas por issues fechados, ou abertos e fechados? (confiabilidade)