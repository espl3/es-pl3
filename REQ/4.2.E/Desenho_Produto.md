Identificador do Requisito: 4.2.E
Nome: Deverá ser possível visualizar a timeline da commits de cada membro activo.
Etapas:

Utilizador entra na pagina inicial
Seleciona Repositório desejado
Seleciona Membros
Seleção do membro em questão
Obter commits filtrados por membro
Visualização de uma timeline onde os commits aparecem por ordem cronológica

Finalidade: Obter todos os commits de cada membro, ordenados numa timeline