Identificador do Requisito: 4.2.E
Nome: Deverá ser possível visualizar a timeline de commits de cada membro ativo.
Descrição: Como developer, preciso criar a visualização da timeline de commits de cada membro ativo. Neste contexto, deverá ser possível ver, por cada commit, quais os ficheiros que foram modificados (nesse commit).
Critérios de Aceitação Funcionais:

A timeline deve conter todos os commits de cada membro, nos quais os ficheiros de cada commit foram enviados, guardados e alterados.
Os commits devem estar bem ordenados consoante à data.

Critérios de Aceitação Não Funcionais:

Os commits devem aparecer por ordem da modificação, isso é, os que sofreram alterações?
Membros que estão fora da equipa, podem ter acesso à timeline?