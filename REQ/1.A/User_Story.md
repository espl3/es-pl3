Identificador do Requisito: 1.A
Nome: Deverá ser possível escolher o repositório GitLab a analisar.
Descrição: Deverá haver uma forma fácil e objetiva de escolher o repositório de maneira a destrinçar os mesmos.
Critérios de Aceitação Funcionais:

A cada repositório deverá corresponder um botão com as características do repositório: imagem, número de commits, número de forks e última atualização.
Devo conseguir observar todos os repositórios a que pertenço.

Critérios de Aceitação Não Funcionais:

Deverão todos os autores ter acesso aos repositórios?
Poderei voltar rapidamente atrás, refazendo a minha escolha, sendo que me enganei na escolha do repositório?