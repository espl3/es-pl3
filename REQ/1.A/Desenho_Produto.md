Identificador do Requisito: 1.A
Nome: Deverá ser possivel escolher o repositório GitLab a analisar
Etapas:

Utilizador entra na página principal
Utilizador vai à sua lista de repositórios
Sistema devolve a lista de repositórios a qual o utilizador tem acesso
Utilizador, perante as opções que tem, escolhe o repositório a analisar
Sistema devolve o repositório escolhido pelo utilizador
Utilizador verá a página do repositório, onde poderá observar algumas caraterísticas à cerca do mesmo como: imagem, cargo no repositório, se é público/privado, número de forks, número de merge requests, número de issues e last updated

Finalidade: De forma mais simples conseguir analisar um repositório pretendido