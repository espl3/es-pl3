Identificador do Requisito: 2.E
Nome: Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por intensidade de alterações.
Descrição: Como utilizador quero que seja apresentada uma lista (tabela) de todos os ficheiros do repositório organizados por intensidade de alterações de forma a estar tudo organizado e para que possa aceder às alterações feitas nos ficheiros.
Critérios de Aceitação Funcionais:

A intensidade de alterações deve estar apresentado numa lista de todos os ficheiros do repositório.

Critérios de Aceitação Não Funcionais:

Como percebemos que os ficheiros foram alterados?
Nas alterações realizadas é indicado o nome de quem realizou a alteração?
Como é que é calculada a intensidade?