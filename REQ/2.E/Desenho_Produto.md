Identificador do Requisito:2.E
Nome: Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por intensidade de alterações.
Etapas:

Utilizador seleciona o seu projeto e acede à página inicial;
Seleciona a página dos ficheiros;
Utilizador visualiza a lista de todos os ficheiros agrupados por intensidade de alterações.

Finalidade: Obter todos os ficheiros agrupados por intensidade de alterações.