Identificador do Requisito: 6.C
Nome: Devera ser possivel ver, por cada commit, quais os ficheiros que foram modificados nesse commit, bem como o respetivo autor.
Descricao: Como membro ativo do projeto, devo ter acesso a uma lista de ficheiros, os quais foram modificados num commit previamente selecionado, sendo identificado o autor do mesmo, de forma a identificar mais facilmente um commit que alterou o ficheiro que me interessa.
Criterios de Aceitacao Funcionais:

Devo conseguir ver o numero de ficheiros alterados por commit?
Devo conseguir ver um commit que foi mais tarde cancelado?

Criterios de Aceitacao Nao Funcionais:

(Disponibilidade) A lista deve ser atualizada sempre que um novo commit e feito ou apenas de, por exemplo, hora em hora?
(Seguranca) Devo conseguir ver todos os commits ou apenas aqueles permitidos?
(Seguranca) Devo ter acesso ao conteudo de cada ficheiro ou apenas ao nome do mesmo?