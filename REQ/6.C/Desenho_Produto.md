Identificador do Requisito: 6.C
Nome: Devera ser possivel ver, por cada commit, quais os ficheiros que foram modificados nesse commit, bem como o respetivo autor.
Etapas:

Utilizador seleciona o projeto em questão;
Segue para a aba de Commits;
Apresentação de todos os commits, ordenados cronologicamente;
Cada commit apresenta os ficheiros alterados incluindo a path dos mesmos no repositório;
Apresenta também o titulo dado a esse commit e o autor do mesmo.

Finalidade: Observar todos os commits, por ordem cronológica, sendo apresentados os ficheiros alterados pelos mesmos e os seus autores.