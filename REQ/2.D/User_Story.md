Identificador do Requisito: 2.D
Nome: Deverá ser apresentada a lista (tabela) de todos os ficheiros do repositório agrupados por autor
Descricao: Como gestor de projeto, pretendo visualizar quais são os ficheiros da autoria de certo utilizador
Criterios de Aceitacao Funcionais:

Acessar os ficheiros da autoria de certo utilizador
Selecionar de que utilizador quero visualizar os ficheiros

Criterios de Aceitacao Nao Funcionais:

Deverá ser possível saber qual a diretoria de cada ficheiro?
Um utilizador com muitos ficheiros criados pode ter mais problemas (em termos de performance) do que um utilizador que tenha poucos ficheiros criados?