"""stormlab_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from StormLab import views
from django.conf.urls import url
from StormLab.controllers import userProfile, commits, issueList, getRepositories, memberlist_controller, filesTree, files, geral

# Aqui adicionar urls de cada página
# O ultimo url é um exemplo de como ser definido
urlpatterns = [
    # path('login/', views.loginPage ),
    url(r'^$', views.loginPage, name='login'),
    url(r'members/user/(?P<username>.*)/(?P<branch>.*)/table/', userProfile.commitsTable.as_view(),
        name='perfil_tabela'),
    url(r'members/user/(?P<username>.*)/(?P<branch>.*)/timeline/', userProfile.commitsTimeline.as_view(),
        name='perfil_timeline'),
    url(r'commits/(?P<pageOrCategory>.*)/(?P<branch>.*)', commits.commitList.as_view(), name='commits'),
    url(r'getRepositories/$', getRepositories.commits.as_view(), name='PáginaPrincipal'),
    url(r'files/(?P<branch>.*)', filesTree.files.as_view(), name='tree'),
    url(r'file/', files.commits.as_view(), name='file_access'),
    url(r'members/', memberlist_controller.memberlist.as_view(), name='membros'),
    url(r'issues/$', issueList.issuesTotal.as_view(), name='issuesTotal'),
    url(r'issues/closed$', issueList.issuesFechados.as_view(), name='issuesFechados'),
    url(r'issues/open$', issueList.issuesAbertos.as_view(), name='issuesAbertos'),
    url(r'geral/$', geral.recent_data.as_view(), name='visaogeral')
]
