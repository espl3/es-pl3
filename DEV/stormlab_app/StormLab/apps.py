from django.apps import AppConfig


class StormlabConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'StormLab'
