let select = document.querySelector(".selecionar div:first-child");
let drop = document.querySelector(".drop");

select.addEventListener("click", function () {
    if (drop.style.display == 'flex') {
        drop.style.display = 'none';
        drop.toggleAttribute('hidden');
    } else {
        drop.style.display = 'flex';
        drop.toggleAttribute('hidden');
    }
});

let options = drop.querySelectorAll("span");


for (let i = 0; i < options.length; i++) {
    options[i].addEventListener("click", function () {
        let newSelection = options[i];
        let newText = newSelection.innerHTML;
        let selection = select.querySelector("span");
        let oldSelection = selection.innerHTML;

        newSelection.innerHTML = oldSelection;
        selection.innerHTML = newText;
        drop.style.display = 'none';
    })
}




