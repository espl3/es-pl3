from django.shortcuts import render, redirect
from django.views.generic import View
from django.http import HttpResponse, HttpResponseRedirect
import gitlab

# Create your views here.

def loginPage(request):
    context = {}
    if request.method == 'POST':
        try:
            gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.POST.get('token'))
            gl.auth()
        except gitlab.exceptions.GitlabAuthenticationError:
            print("DEBUG: TOKEN INVALIDO")
            # redirect deve ser para uma pagina a dizer erro
            context = {
                'token': 'invalid'
            }
            return render(request, 'login.html', context)

        response = HttpResponseRedirect('getRepositories/')
        response.set_cookie('token', request.POST.get('token'))
        return response

    elif request.method == 'GET':
        return render(request,'login.html',context)

def header(request):
    gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
    gl.auth()
    project = gl.projects.get(int(request.COOKIES.get('projectID')))

    auxlong = project.branches.list(all=True)

    result = {
        'projectname' : project.name,
        'project_img' : project.avatar_url,
        'id': gl.user.id,
        'branches': len(auxlong),
        'commits': len(project.commits.list(all=True)),
        'username': gl.user.name,
        'actual_username': gl.user.username,
        'avatar': gl.user.avatar_url,
        'branchesOptions':  auxlong
    }

    return result