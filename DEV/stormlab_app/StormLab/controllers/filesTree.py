from datetime import datetime

from django.shortcuts import render
import gitlab
import re
from django.views.generic import View
from StormLab import views


class files(View):
    def get(self, request, branch):

        gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
        gl.auth()
        project = gl.projects.get(int(request.COOKIES.get('projectID')))

        if request.method == 'GET':
            ficheiros_lista = project.repository_tree(path=request.GET.get("path"), ref=branch, all=True)

        first_file = project.repository_tree(ref=branch, page=1, per_page=1)

        commit = project.commits.list(per_page=1, page=1)[0]
        name = commit.author_name
        title = commit.title
        image = None
        for member in project.members_all.list(all=True):
            if member.name == commit.author_name:
                image = member.avatar_url
                break
        aux = commit.committed_date

        if len(aux.split(",")) == 1:
            data = "{} {}".format(aux.split("T")[0], aux.split("T")[1].split(".")[0])
            lastUpdate = datetime.today() - datetime.strptime(data, '%Y-%m-%d %H:%M:%S')

            if len(str(lastUpdate).split(",")) == 1:
                [horas, minutos] = [str(lastUpdate).split(":")[0], str(lastUpdate).split(":")[1]]
                if minutos[0] == '0':
                    minutos = minutos[1]

                if horas == "0":
                    if minutos == "0":
                        string = "menos de 1 minuto"
                    elif minutos == "1":
                        string = "1 minuto"
                    else:
                        string = "{} minutos".format(minutos)
                else:
                    if horas == "1":
                        string = "1 hora"
                    else:
                        string = "{} horas".format(horas)

                    if minutos == "0":
                        pass
                    elif minutos == "1":
                        string += " e 1 minuto"
                    else:
                        string += " e {} minutos".format(minutos)

            else:
                string = "{} dias".format(str(lastUpdate).split(",")[0].split(" ")[0])

        result = {
            "first_file": first_file[0]["path"],
            "my_count": ficheiros_lista,
            "branch": branch,
            "header": views.header(request),
            "name": name,
            "commitTitle": title,
            "image": image,
            "lastUpdate": string
        }
        print(request.GET.get("path"))
        return render(request, 'TabelaFicheiros.html', result)
