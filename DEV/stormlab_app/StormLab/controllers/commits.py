from datetime import datetime

from django.shortcuts import render
from django.views.generic import View
from StormLab import views

import gitlab


class commitList(View):
    def get(self, request, pageOrCategory, branch):
        token = request.COOKIES.get('token')
        project = request.COOKIES.get('projectID')

        gl = gitlab.Gitlab('https://gitlab.com/', private_token=token)
        gl.auth()

        commits = []
        listaCommits = []
        listaDias = []
        categories = ['ARCH', 'DES', 'DEV', 'ENV', 'PM', 'PROC', 'PROD', 'QA', 'REQ', 'TST']

        if pageOrCategory not in categories:
            try:
                commits = gl.projects.get(int(project)).commits.list(ref_name=branch, per_page=10, page=int(pageOrCategory))
            except ValueError:
                exit(0)
        else:
            commits = gl.projects.get(int(project)).commits.list(all=True, query_parameters={'ref_name':branch})

        ultimo_dia = commits[0].committed_date[0:10]

        for i, commit in enumerate(commits):
            if commit.committed_date[0:10] != ultimo_dia:
                diaDic = {"data": datetime.strptime(ultimo_dia[0:10], '%Y-%m-%d').strftime('%d-%m-%Y'), 'nCommits': len(listaCommits), 'commits': listaCommits}
                ultimo_dia = commit.committed_date[0:10]
                if len(listaCommits) > 0:
                    listaDias.append(diaDic)
                    listaCommits = []

            commitDic = {}
            commitDic['titulo'] = commit.title
            commitDic['nome'] = commit.author_name
            commitDic['data'] = commit.committed_date[11:16]
            for member in gl.projects.get(int(project)).members_all.list(all=True):
                if member.name == commit.author_name:
                    commitDic['imagem'] = member.avatar_url
                    break

            list = commit.diff()
            if list:
                if len(list) > 1:
                    commitDic['ficheiros'] = '{} + {} outros ficheiros'.format(list[0].get('new_path'), str(len(list) - 1))
                else:
                    commitDic['ficheiros'] = '{}'.format(list[0].get('new_path'))
            else:
                commitDic['ficheiros'] = 'Sem ficheiros alterados'

            if pageOrCategory in categories:
                for dic in list:
                    if dic.get('new_path').split('/')[0] == pageOrCategory:
                        listaCommits.append(commitDic)
                        break
            else:
                listaCommits.append(commitDic)

            if i == 9:
                diaDic = {"data": datetime.strptime(ultimo_dia[0:10], '%Y-%m-%d').strftime('%d-%m-%Y'), 'nCommits': len(listaCommits), 'commits': listaCommits}
                ultimo_dia = commit.committed_date[0:10]
                if len(listaCommits) > 0:
                    listaDias.append(diaDic)
                    listaCommits = []

        aux = commits[0].committed_date
        if len(aux.split(",")) == 1:
            data = "{} {}".format(aux.split("T")[0], aux.split("T")[1].split(".")[0])
            lastUpdate = datetime.today() - datetime.strptime(data, '%Y-%m-%d %H:%M:%S')

            if len(str(lastUpdate).split(",")) == 1:
                [horas, minutos] = [str(lastUpdate).split(":")[0], str(lastUpdate).split(":")[1]]
                if minutos[0] == '0':
                    minutos = minutos[1]

                if horas == "0":
                    if minutos == "0":
                        string = "menos de 1 minuto"
                    elif minutos == "1":
                        string = "1 minuto"
                    else:
                        string = "{} minutos".format(minutos)
                else:
                    if horas == "1":
                        string = "1 hora"
                    else:
                        string = "{} horas".format(horas)

                    if minutos == "0":
                        pass
                    elif minutos == "1":
                        string += " e 1 minuto"
                    else:
                        string += " e {} minutos".format(minutos)

            else:
                string = "{} dias".format(str(lastUpdate).split(",")[0].split(" ")[0])

        result = {'dias': listaDias,
                  'header': views.header(request),
                  'lastUpdate': string,
                  "branch":branch}

        return render(request, 'commits.html', result)
