import gitlab
import json
from django.http.response import JsonResponse
from django.shortcuts import render
from django.views.generic import View
from datetime import datetime
from StormLab import views
class recent_data(View):

    def get(self, request):
        token = request.COOKIES.get('token')
        project = request.COOKIES.get('projectID')

        gl = gitlab.Gitlab('https://gitlab.com/', private_token=token)
        gl.auth()

        listaCommits = []
        listaDias = []
        listaIssues = []
        listaDiasIssues = []

        commits = gl.projects.get(int(project)).commits.list(page=1, per_page=10)
        issues_total = gl.projects.get(int(project)).issues.list(page=1, per_page=10, order_by='updated_at',sort='desc')
        if commits[0].committed_date < issues_total[0].updated_at:
            aux = commits[0].committed_date
        else:
            aux = issues_total[0].updated_at

        ultimo_dia = commits[0].committed_date[0:10]

        for i, commit in enumerate(commits):
            if commit.committed_date[0:10] != ultimo_dia:
                diaDic = {"data": datetime.strptime(ultimo_dia[0:10], '%Y-%m-%d').strftime('%d-%m-%Y'), 'nCommits': len(listaCommits), 'commits': listaCommits}
                ultimo_dia = commit.committed_date[0:10]
                if len(listaCommits) > 0:
                    listaDias.append(diaDic)
                    listaCommits = []

            commitDic = {}
            commitDic['titulo'] = commit.title
            commitDic['nome'] = commit.author_name
            commitDic['data'] = commit.committed_date[11:16]
            for member in gl.projects.get(int(project)).members_all.list(all=True):
                if member.name == commit.author_name:
                    commitDic['imagem'] = member.avatar_url
                    break

            listaCommits.append(commitDic)
            if i == 9:
                diaDic = {"data": datetime.strptime(ultimo_dia[0:10], '%Y-%m-%d').strftime('%d-%m-%Y'), 'nCommits': len(listaCommits), 'commits': listaCommits}
                ultimo_dia = commit.committed_date[0:10]
                if len(listaCommits) > 0:
                    listaDias.append(diaDic)
                    listaCommits = []


        ultimo_dia_issues = issues_total[0].updated_at[0:10]
        for j, issue in enumerate(issues_total):
            if issue.updated_at[0:10] != ultimo_dia_issues:
                diaDicI = {"data": datetime.strptime(ultimo_dia[0:10], '%Y-%m-%d').strftime('%d-%m-%Y'),'nIssues': len(listaIssues), 'issues': listaIssues}
                ultimo_dia = issue.updated_at[0:10]
                if len(listaIssues) > 0:
                    listaDiasIssues.append(diaDicI)
                    listaIssues = []

            commitDic = {}
            commitDic['titulo'] = issue.title
            commitDic['nome'] = issue.author.get('name')
            commitDic['data'] = issue.updated_at[11:16]
            commitDic['imagem'] = issue.author.get('avatar_url')

            listaIssues.append(commitDic)

        if j == 9:
            diaDicI = {"data": datetime.strptime(ultimo_dia[0:10], '%Y-%m-%d').strftime('%d-%m-%Y'),
                       'nIssues': len(listaIssues), 'issues': listaIssues}
            ultimo_dia = issue.updated_at[0:10]
            if len(listaIssues) > 0:
                listaDiasIssues.append(diaDicI)
                listaIssues = []

        if len(aux.split(",")) == 1:
            data = "{} {}".format(aux.split("T")[0], aux.split("T")[1].split(".")[0])
            lastUpdate = datetime.today() - datetime.strptime(data, '%Y-%m-%d %H:%M:%S')

            if len(str(lastUpdate).split(",")) == 1:
                [horas, minutos] = [str(lastUpdate).split(":")[0], str(lastUpdate).split(":")[1]]
                if minutos[0] == '0':
                    minutos = minutos[1]

                if horas == "0":
                    if minutos == "0":
                        string = "menos de 1 minuto"
                    elif minutos == "1":
                        string = "1 minuto"
                    else:
                        string = "{} minutos".format(minutos)
                else:
                    if horas == "1":
                        string = "1 hora"
                    else:
                        string = "{} horas".format(horas)

                    if minutos == "0":
                        pass
                    elif minutos == "1":
                        string += " e 1 minuto"
                    else:
                        string += " e {} minutos".format(minutos)

            else:
                string = "{} dias".format(str(lastUpdate).split(",")[0].split(" ")[0])

        result = {
            'committs': listaDias,
            'issues': listaDiasIssues,
            'header': views.header(request),
            'lastUpdate': string
        }

        #print(listaDias)
        #print(listaDiasIssues)

        return render(request, 'visaogeral.html', result)

