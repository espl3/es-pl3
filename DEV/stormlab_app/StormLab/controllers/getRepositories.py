import gitlab
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponseRedirect
from datetime import datetime
from StormLab import views

class commits(View):
    def get(self, request):
        gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
        gl.auth()

        list_projects=[]

        for proj in gl.projects.list(membership=True):
            aux = proj.last_activity_at

            data = "{} {}".format(aux.split("T")[0], aux.split("T")[1].split(".")[0])
            lastUpdate = datetime.today() - datetime.strptime(data, '%Y-%m-%d %H:%M:%S')

            if len(str(lastUpdate).split(",")) == 1:
                [horas, minutos] = [str(lastUpdate).split(":")[0], str(lastUpdate).split(":")[1]]
                if minutos[0] == '0':
                    minutos = minutos[1]

                if horas == "0":
                    if minutos == "0":
                        string = "menos de 1 minuto"
                    elif minutos == "1":
                        string = "1 minuto"
                    else:
                        string = "{} minutos".format(minutos)
                else:
                    if horas == "1":
                        string = "1 hora"
                    else:
                        string = "{} horas".format(horas)

                    if minutos == "0":
                        pass
                    elif minutos == "1":
                        string += " e 1 minuto"
                    else:
                        string += " e {} minutos".format(minutos)

            else:
                string = "{} dias".format(str(lastUpdate).split(",")[0].split(" ")[0])

            list_projects += [[proj, len(proj.commits.list(all=True)), len(proj.branches.list(all=True)), str(string).split(".")[0] ]]

        return render(request, 'getRepositories.html', {'list_projects':list_projects})

    def post(self, request):
        id = request.POST


        response = HttpResponseRedirect('http://10.17.0.174:8080/geral/')
        response.set_cookie('projectID', id['projeto'])

        return response