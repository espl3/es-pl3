from operator import itemgetter

import gitlab
import json
from django.http.response import JsonResponse
from django.shortcuts import render
from django.views.generic import View
from StormLab import views


class memberlist(View):

    def get(self, request):

        allowedusers = []

        gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
        project = gl.projects.get(int(request.COOKIES.get('projectID')))

        for member in project.members_all.list(all=True):
            for commit in project.commits.list(all=True):
                if member.name == commit.author_name:
                    allowedusers.append(member)
                    break


        
        allowed_users_names = [0] * len(allowedusers)
        i = 0
        for member in allowedusers:
            allowed_users_names[i] = member.name
            i += 1

        allowed_users_names.sort()
        total = 0
        total_commits = 0

        count_lines_changed = [0] * 10
        count_commits = [0] * 10
       
        arrayCategoryDici = []
        for k in range(len(allowedusers)):
            arrayCategoryDici.append({
                "ARCH": 0,
                "DES": 0,
                "DEV": 0,
                "ENV": 0,
                "PM": 0,
                "PROC": 0,
                "PROD": 0,
                "QA": 0,
                "REQ": 0,
                "TST": 0
            })

        for commit in project.commits.list(all=True, with_stats=True):
            if commit.diff():

                aux = 0
                for member in allowed_users_names:
                    if member == commit.author_name:
                        aux = allowed_users_names.index(member)
                        break
              
                for i, dic in enumerate(commit.diff()):
                    if i == 0:
                        total += commit.stats.get('total')
                        total_commits += 1
                        x = dic.get('new_path').split('/')

                        if x[0] == 'ARCH':
                            count_commits[0] += 1
                            count_lines_changed[0] += commit.stats.get('total')
                            arrayCategoryDici[aux]["ARCH"] += 1
                        elif x[0] == 'DES':
                            count_commits[1] += 1
                            count_lines_changed[1] += commit.stats.get('total')
                            arrayCategoryDici[aux]["DES"] += 1
                        elif x[0] == 'DEV':
                            count_commits[2] += 1
                            count_lines_changed[2] += commit.stats.get('total')
                            arrayCategoryDici[aux]["DEV"] += 1
                        elif x[0] == 'ENV':
                            count_commits[3] += 1
                            count_lines_changed[3] += commit.stats.get('total')
                            arrayCategoryDici[aux]["ENV"] += 1
                        elif x[0] == 'PM':
                            count_commits[4] += 1
                            count_lines_changed[4] += commit.stats.get('total')
                            arrayCategoryDici[aux]["PM"] += 1
                        elif x[0] == 'PROC':
                            count_commits[5] += 1
                            count_lines_changed[5] += commit.stats.get('total')
                            arrayCategoryDici[aux]["PROC"] += 1
                        elif x[0] == 'PROD':
                            count_commits[6] += 1
                            count_lines_changed[6] += commit.stats.get('total')
                            arrayCategoryDici[aux]["PROD"] += 1
                        elif x[0] == 'QA':
                            count_commits[7] += 1
                            count_lines_changed[7] += commit.stats.get('total')
                            arrayCategoryDici[aux]["QA"] += 1
                        elif x[0] == 'REQ':
                            count_commits[8] += 1
                            count_lines_changed[8] += commit.stats.get('total')
                            arrayCategoryDici[aux]["REQ"] += 1
                        elif x[0] == 'TST':
                            count_commits[9] += 1
                            count_lines_changed[9] += commit.stats.get('total')
                            arrayCategoryDici[aux]["TST"] += 1
                        break

        

        arrayRoles = []

        for i in range(len(allowed_users_names)):
            cat = max(arrayCategoryDici[i], key=arrayCategoryDici[i].get)
            arrayRoles.append(cat)

        table = {}
        for key in allowed_users_names:
            for value in arrayRoles:
                table[key] = value
                arrayRoles.remove(value)
                break


        dici = {
            "ARCH": [count_lines_changed[0], count_commits[0]],
            "DES": [count_lines_changed[1], count_commits[1]],
            "DEV": [count_lines_changed[2], count_commits[2]],
            "ENV": [count_lines_changed[3], count_commits[3]],
            "PM": [count_lines_changed[4], count_commits[4]],
            "PROC": [count_lines_changed[5], count_commits[5]],
            "PROD": [count_lines_changed[6], count_commits[6]],
            "QA": [count_lines_changed[7], count_commits[7]],
            "REQ": [count_lines_changed[8], count_commits[8]],
            "TST": [count_lines_changed[9], count_commits[9]]
        }
        d = {
            "infos": dici,
            "header": views.header(request),
            "names": allowed_users_names,
            "total_lines_changed": total,
            "total_commits": total_commits,
            "data": sorted(table.items())
        }

        return render(request, 'membros.html', d)
