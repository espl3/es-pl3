from django.shortcuts import render
import gitlab
from datetime import datetime
from StormLab import views
from django.views.generic import View

class issuesTotal(View):
    def get(self,request):
        gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
        gl.auth()

        project = gl.projects.get(int(request.COOKIES.get('projectID')))
        numero_total = project.issues_statistics.get()

        maxpage = numero_total.statistics["counts"]["all"] / 10

        page = request.GET.get("previous")
        pages = request.GET.get("next")

        if page is None and pages is None:
            page = 1
        elif page is not None and pages is None:
            if int(page) < 1:
                page = 1
            elif int(page) >= maxpage:
                page = maxpage - 1
            else:
                page = int(page) + 1
        elif page is None and pages is not None:
            if int(pages) <= 1:
                pages = 1
            elif int(pages) > maxpage:
                pages = maxpage
            else:
                pages = int(pages) - 1
            page = pages

        issues_total = project.issues.list(page=page, per_page=10, order_by='created_at', sort='desc')
        if(len(project.issues.list(page=1, per_page=1, order_by='updated_at', sort='desc'))>0):
            aux = project.issues.list(page=1, per_page=1, order_by='updated_at', sort='desc')[0].updated_at
            if len(aux.split(",")) == 1:
                data = "{} {}".format(aux.split("T")[0], aux.split("T")[1].split(".")[0])
                lastUpdate = datetime.today() - datetime.strptime(data, '%Y-%m-%d %H:%M:%S')

                if len(str(lastUpdate).split(",")) == 1:
                    [horas, minutos] = [str(lastUpdate).split(":")[0], str(lastUpdate).split(":")[1]]
                    if minutos[0] == '0':
                        minutos = minutos[1]

                    if horas == "0":
                        if minutos == "0":
                            string = "menos de 1 minuto"
                        elif minutos == "1":
                            string = "1 minuto"
                        else:
                            string = "{} minutos".format(minutos)
                    else:
                        if horas == "1":
                            string = "1 hora"
                        else:
                            string = "{} horas".format(horas)

                        if minutos == "0":
                            pass
                        elif minutos == "1":
                            string += " e 1 minuto"
                        else:
                            string += " e {} minutos".format(minutos)

                else:
                    string = "{} dias".format(str(lastUpdate).split(",")[0].split(" ")[0])

        else:
            string = "----"


        for i in issues_total:
            if i.due_date is not None:
                i.due_date = i.due_date[:10]
                i.due_date = datetime.strptime(i.due_date, '%Y-%m-%d').strftime('%d-%m-%Y')
            if i.created_at is not None:
                i.created_at = i.created_at[:10]
                i.created_at = datetime.strptime(i.created_at, '%Y-%m-%d').strftime('%d-%m-%Y')
            i.updated_at = i.updated_at[:10]+' | '+i.updated_at[11:19]



        result = {
            "numero_total": numero_total,
            "my_issues_total": issues_total,
            "header": views.header(request),
            "my_page": page,
            "lastUpdateTotal": string
        }


        return render(request, 'issuesTotal.html', result)

class issuesFechados(View):

    def get(self,request):
        gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
        gl.auth()

        project = gl.projects.get(int(request.COOKIES.get('projectID')))

        numero_total = project.issues_statistics.get()

        maxpage = numero_total.statistics["counts"]["closed"] / 10

        page = request.GET.get("previous")
        pages = request.GET.get("next")

        if page is None and pages is None:
            page = 1
        elif page is not None and pages is None:
            if int(page) < 1:
                page = 1
            elif int(page) >= maxpage:
                page = maxpage
            else:
                page = int(page) + 1
        elif page is None and pages is not None:
            if int(pages) <= 1:
                pages = 1
            elif int(pages) > maxpage:
                pages = maxpage
            else:
                pages = int(pages) - 1
            page = pages

        issues_fechados = project.issues.list(state="closed", page=page, per_page=10, order_by='created_at', sort='desc')

        if(len(project.issues.list(state="closed", page=1, per_page=1, order_by='updated_at', sort='desc'))>0):
            aux = project.issues.list(state="closed", page=1, per_page=1, order_by='updated_at', sort='desc')[0].updated_at

            if len(aux.split(",")) == 1:
                data = "{} {}".format(aux.split("T")[0], aux.split("T")[1].split(".")[0])
                lastUpdate = datetime.today() - datetime.strptime(data, '%Y-%m-%d %H:%M:%S')

                if len(str(lastUpdate).split(",")) == 1:
                    [horas, minutos] = [str(lastUpdate).split(":")[0], str(lastUpdate).split(":")[1]]
                    if minutos[0] == '0':
                        minutos = minutos[1]

                    if horas == "0":
                        if minutos == "0":
                            string = "menos de 1 minuto"
                        elif minutos == "1":
                            string = "1 minuto"
                        else:
                            string = "{} minutos".format(minutos)
                    else:
                        if horas == "1":
                            string = "1 hora"
                        else:
                            string = "{} horas".format(horas)

                        if minutos == "0":
                            pass
                        elif minutos == "1":
                            string += " e 1 minuto"
                        else:
                            string += " e {} minutos".format(minutos)

                else:
                    string = "{} dias".format(str(lastUpdate).split(",")[0].split(" ")[0])

        else:
            string = "----"

        for i in issues_fechados:
            if i.due_date is not None:
                i.due_date = i.due_date[:10]
                i.due_date = datetime.strptime(i.due_date, '%Y-%m-%d').strftime('%d-%m-%Y')
            if i.created_at is not None:
                i.created_at = i.created_at[:10]
                i.created_at = datetime.strptime(i.created_at, '%Y-%m-%d').strftime('%d-%m-%Y')
            i.updated_at = i.updated_at[:10] + ' | ' + i.updated_at[11:19]



        result = {
            "numero_total": numero_total,
            "my_issues_fechados": issues_fechados,
            "header": views.header(request),
            "my_page": page,
            "lastUpdateClosed": string
        }
        return render(request, 'issuesFechados.html', result)

class issuesAbertos(View):

    def get(self,request):
        gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
        gl.auth()

        project = gl.projects.get(int(request.COOKIES.get('projectID')))

        numero_total = project.issues_statistics.get()

        maxpage = numero_total.statistics["counts"]["opened"] / 10

        page = request.GET.get("previous")
        pages = request.GET.get("next")

        if page is None and pages is None:
            page = 1
        elif page is not None and pages is None:
            if int(page) < 1:
                page = 1
            elif int(page) >= maxpage:
                page = maxpage
            else:
                page = int(page) + 1
        elif page is None and pages is not None:
            if int(pages) <= 1:
                pages = 1
            elif int(pages) > maxpage:
                pages = maxpage
            else:
                pages = int(pages) - 1
            page = pages

        issues_abertos = project.issues.list(state="opened", page=page, per_page=10, order_by='created_at', sort='desc')

        if(len(project.issues.list(state="opened", page=1, per_page=1, order_by='updated_at', sort='desc'))>0):
            aux = project.issues.list(state="opened", page=1, per_page=1, order_by='updated_at', sort='desc')[0].updated_at

            if len(aux.split(",")) == 1:
                data = "{} {}".format(aux.split("T")[0], aux.split("T")[1].split(".")[0])
                lastUpdate = datetime.today() - datetime.strptime(data, '%Y-%m-%d %H:%M:%S')

                if len(str(lastUpdate).split(",")) == 1:
                    [horas, minutos] = [str(lastUpdate).split(":")[0], str(lastUpdate).split(":")[1]]
                    if minutos[0] == '0':
                        minutos = minutos[1]

                    if horas == "0":
                        if minutos == "0":
                            string = "menos de 1 minuto"
                        elif minutos == "1":
                            string = "1 minuto"
                        else:
                            string = "{} minutos".format(minutos)
                    else:
                        if horas == "1":
                            string = "1 hora"
                        else:
                            string = "{} horas".format(horas)

                        if minutos == "0":
                            pass
                        elif minutos == "1":
                            string += " e 1 minuto"
                        else:
                            string += " e {} minutos".format(minutos)

                else:
                    string = "{} dias".format(str(lastUpdate).split(",")[0].split(" ")[0])

        else:
            string = "----"

        for i in issues_abertos:
            if i.due_date is not None:
                i.due_date = i.due_date[:10]
                i.due_date = datetime.strptime(i.due_date, '%Y-%m-%d').strftime('%d-%m-%Y')
            if i.created_at is not None:
                i.created_at = i.created_at[:10]
                i.created_at = datetime.strptime(i.created_at, '%Y-%m-%d').strftime('%d-%m-%Y')
            i.updated_at = i.updated_at[:10] + ' | ' + i.updated_at[11:19]



        result = {
            "numero_total": numero_total,
            "my_issues_abertos": issues_abertos,
            "header": views.header(request),
            "my_page": page,
            "lastUpdateOpened": string
        }

        return render(request, 'issuesAbertos.html', result)

