import gitlab
from datetime import datetime,timedelta
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import View
from StormLab import views

def perfil(request, username):
    gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
    gl.auth()
    project = gl.projects.get(int(request.COOKIES.get('projectID')))
    members = project.members_all.list(all=True)
    for member in members:
        if member.username == username:
            user = member

    valor = user.access_level
    cat_level = ""
    if valor == 40:
        cat_level = "Maintainer"
    elif valor == 30:
        cat_level = "Developer"
    elif valor == 20:
        cat_level = "Reporter"
    elif valor == 10:
        cat_level = "Guest"
    elif valor == 5:
        cat_level = "Minimal access"
    elif valor == 0:
        cat_level = "No access"
    elif valor == 50:
        cat_level = "Owner"

    context = {
        "objet": user,
        # 'username': user.username,
        # 'avatar': user.avatar_url,
        # 'id': user.id,
        "cargo": cat_level

    }

    return context


class commitsTable(View):

    def get(self, request,username, branch):

        gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
        gl.auth()
        project = gl.projects.get(int(request.COOKIES.get('projectID')))

        profile = perfil(request, username)

        author_name = profile.get('objet').name

        commitsArray = []

        commits = project.commits.list(all=True, ref_name=branch)
        for commit in commits:
            if commit.author_name == author_name:
                commitsArray += [commit]
        result = {
            "user": username,
            "branch": branch,
            "commitsArray": commitsArray,
            "header": views.header(request),
            "perfil": profile
        }


        return render(request, 'perfil_tabela.html',result)


class commitsTimeline(View):

    def get(self, request, username, branch):

        gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
        gl.auth()
        project = gl.projects.get(int(request.COOKIES.get('projectID')))

        profile = perfil(request, username)

        author_name = profile.get('objet').name

        commitsArray = []

        commits = project.commits.list(all=True ,query_parameters={'ref_name':branch})
        for commit in commits:
            if commit.author_name == author_name:
                info = []
                titulo = commit.title
                aux = commit.created_at
                data = aux.split("T")[0]
                hora = aux.split("T")[1].split(".")[0][0:5]
                info += [titulo, data, hora]
                if commit.diff():
                    ficheiros = []
                    for i, dic in enumerate(commit.diff()):
                        ficheiros += [project.path+"/"+dic.get('new_path')]
                    info += [ficheiros]
                commitsArray += [info]

        result = {"user":username,
                  "branch":branch,
                  "commitsArray":commitsArray,
                  "header": views.header(request),
                  "perfil": profile
                  }

        return render(request, 'perfil_timeline.html', result)
