from datetime import datetime

import gitlab
import json
from django.http.response import JsonResponse
from django.shortcuts import render
from django.views.generic import View
from StormLab import views

class commits(View):

    def get(self,request,):

        gl = gitlab.Gitlab('https://gitlab.com/', private_token=request.COOKIES.get('token'))
        gl.auth()

        branch = 'main' # quando existir chamar token com o branch
        path = request.GET.get("filepath")
        project = gl.projects.get(int(request.COOKIES.get('projectID')))
        commits = project.commits.list(all=True ,ref_name = branch,with_stats=True)
        commitsArray = []
        lista = []
        prevdate = ""
        au2 = None
        aux3 = False
        for commit in commits:
            if commit.diff():
                for i, dic in enumerate(commit.diff()):
                    # segundo o mario o match com newpath apesar de ser string diferente este commit alterou ficheiro
                    if path == dic.get('new_path'):
                        if aux3 == False:
                            aux2 = commit.committed_date
                            aux3 = True
                        date = "{} 0:0:0".format(commit.committed_date.split("T")[0])
                        commit.committed_date = "{}".format(commit.committed_date.split("T")[0])
                        if prevdate== "":
                            prevdate=date

                        if prevdate!=date:
                            print(prevdate,date)
                            prevdate=date
                            commitsArray += [lista]
                            lista = []

                        lista += [commit]
                        break

        aux = path.split("/")
        filename = aux[len(aux)-1]
        filepath = ""
        for i in range(len(aux)-1):
            filepath += aux[i]+"/"

        if len(aux2.split(",")) == 1:
            data = "{} {}".format(aux2.split("T")[0], aux2.split("T")[1].split(".")[0])
            lastUpdate = datetime.today() - datetime.strptime(data, '%Y-%m-%d %H:%M:%S')

            if len(str(lastUpdate).split(",")) == 1:
                [horas, minutos] = [str(lastUpdate).split(":")[0], str(lastUpdate).split(":")[1]]
                if minutos[0] == '0':
                    minutos = minutos[1]

                if horas == "0":
                    if minutos == "0":
                        string = "menos de 1 minuto"
                    elif minutos == "1":
                        string = "1 minuto"
                    else:
                        string = "{} minutos".format(minutos)
                else:
                    if horas == "1":
                        string = "1 hora"
                    else:
                        string = "{} horas".format(horas)

                    if minutos == "0":
                        pass
                    elif minutos == "1":
                        string += " e 1 minuto"
                    else:
                        string += " e {} minutos".format(minutos)

            else:
                string = "{} dias".format(str(lastUpdate).split(",")[0].split(" ")[0])

        result = {'commits':commitsArray,
                  'filepath': filepath,
                  'filename':filename,
                  'header': views.header(request),
                  'lastUpdate': string }

        return render(request, 'file_access.html',result)



