# **Cargos Principais**

## Gestores de Projeto

- [`João Monteiro`](https://gitlab.com/joaolopix/) 
- [`Nathalie Jennings`](https://gitlab.com/nathaliepintoj/)

## Secretariado

- [`Bárbara Teixeira`](https://gitlab.com/barbaradpteixeira/): Líder
- [`Ana Carvalho`](https://gitlab.com/aiccarvalho2008/)
- [`Beatriz Ferreira`](https://gitlab.com/bferreira27/)
- [`Filipa Nogueira`](https://gitlab.com/filipa.nogueira2015/)
- [`Susana Batista`](https://gitlab.com/susana.pires.batista/)

## Controlo de Qualidade

- [`João Silva`](https://gitlab.com/JoSilva21/): Líder
- [`António Correia`](https://gitlab.com/ImCorry/)
- [`Francisco Pires`](https://gitlab.com/fjrp12/)
- [`João Santos`](https://gitlab.com/joaosantosnatal/)
- [`Pedro Letra`](https://gitlab.com/pmiguelcletra/)
- [`Pedro Martins`](https://gitlab.com/PedroUCMartins/)

## Arquitetos

- [`Ana Gil`](https://gitlab.com/anagofof/)
- [`Bruno Marques`](https://gitlab.com/bruno_msm/)
- [`João Melo`](https://gitlab.com/joMelo01/)
- [`Miguel Faria`](https://gitlab.com/miguel_faria_57/)

## Front-End

- [`Luís Gonçalves`](https://gitlab.com/lalexgoncalves): Líder
- [`Nathalie Jennings`](https://gitlab.com/nathaliepintoj/)
- [`Bárbara Teixeira`](https://gitlab.com/barbaradpteixeira/)
- [`Ana Carvalho`](https://gitlab.com/aiccarvalho2008/)
- [`Beatriz Ferreira`](https://gitlab.com/bferreira27/)
- [`Filipa Nogueira`](https://gitlab.com/filipa.nogueira2015/)
- [`Susana Batista`](https://gitlab.com/susana.pires.batista/)
- [`João Santos`](https://gitlab.com/joaosantosnatal/)
- [`Pedro Letra`](https://gitlab.com/pmiguelcletra/)
- [`Ana Gil`](https://gitlab.com/anagofof/)
- [`André Queirós`](https://gitlab.com/Queiros)
- [`Dumitru Brailean`](https://gitlab.com/bradima07)
- [`João Matos`](https://gitlab.com/jpnortem)

## Back-End

- [`Alexandre van Velze`](https://gitlab.com/alexvvelze): Líder
- [`João Monteiro`](https://gitlab.com/joaolopix/)
- [`João Silva`](https://gitlab.com/JoSilva21/)
- [`António Correia`](https://gitlab.com/ImCorry/)
- [`Francisco Pires`](https://gitlab.com/fjrp12/)
- [`Pedro Martins`](https://gitlab.com/PedroUCMartins/)
- [`Bruno Marques`](https://gitlab.com/bruno_msm/)
- [`João Melo`](https://gitlab.com/joMelo01/)
- [`David Forte`](https://gitlab.com/David_F)
- [`Duarte Fonte-Santa`](https://gitlab.com/DuarteFonte_Santa)
- [`Fábio Vaqueiro`](https://gitlab.com/fabiomvaqueiro)
- [`Mário Lemos`](https://gitlab.com/Mario_Lemos)
- [`Miguel Faria`](https://gitlab.com/miguel_faria_57) (Líder Python)

## DevOps

- [`Alexandre van Velze`](https://gitlab.com/alexvvelze)
- [`Mário Lemos`](https://gitlab.com/Mario_Lemos)
- [`Fábio Vaqueiro`](https://gitlab.com/fabiomvaqueiro)

