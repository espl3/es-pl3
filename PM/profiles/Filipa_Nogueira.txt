Nome:Filipa Mendes Nogueira
Número de estudante: 2019212940
Email: uc2019212940@student.uc.pt
ID no GitLab: filipa.nogueira2015
Cargos:Front-end developer / secretariado

Contribuições:

ARCH:
-Desenvolvimento da identidade visual (issue #171)
-Desenvolvimento dos layouts do site (issues #215,#267,#304)
-Decidir convenções do css a utilizar (issue #358)

DEV:
-Desenvolvimento do front-end das páginas file tree (issue #388)
-Desenvolvimento dos requisitos 2.A, 2.B e 2.C e integração com a parte de Front-End (issue #504)
-Implementação do header na página de issues (issue #467)
-Implementação do dropdown na página dos ficheiros (issue #565)

PM:
-Criação do ficheiro profile (issue #39)
-Realização de atas (issues #71, #175, #244, #387, #484)
-Colocação de atas no repositório (issue #518)
-Atualização do ficheiro profile (issue #524)


PROC:
-Aprendizagem de Git e Gitlab (issue #11)
-Aprendizagem de Python (issue #65)
-Aprendizagem de Django (issue #111)


REQ:
-Criação do userstory correspondente ao requisito 2.C (issue #129)
-Verificação do user story 2.E (issue #186)
-Desenho do produto do requisito 2.C (issue #313)


TST:
-Teste do requisito 5.A (issue #495)


Papel principal no projeto: Fiz parte do secretariado e fui Front_end developer

