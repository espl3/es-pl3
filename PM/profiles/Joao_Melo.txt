Nome: João António Da Silva Melo
Número de estudante: 2019216747
Email: uc2019216747@student.uc.pt
ID no GitLab: 
Cargos: Backend Developer / Arquitetura

Contribuições:


ARCH:
- Discussão inicial da Arquitetura para discutir o desenvolvimento desta de acordo com o modelo C4 e informações pesquisadas.(Issue #245).
- Desenvolvimento da arquitetura do site e na escolha das bibliotecas python a utilizar. (Issue #276 e 291).
- Revisão final da Arquitetura do site para verificar e modificar certas alterações feitas. Finalização do nível 4 do modelo C4 
(Issue #514).


DEV:

- Desenvolvimento do conteúdo Back-End do requisito 4.1.C (Deverá ser possível visualizar o esforço total aplicado no projeto, por categoria (REQ, PM, DEV, TST...)) e o seu respetivo controller (Issue #383).
- Iniciação dos conteúdos fundamentais a utilizar no website (Funcionamento com a API do gitlab recorrendo a Python-Gitlab) (Issue #382).
- Desenvolvimento parcial do Requisito 3.C (A informação estática sobre o ficheiro) (Issue #532).
- Funcionalidade de token para aceder aos repositórios GitLab de um utilizador e armazenamento destes dados numa Cookie para mais fácil acesso (Issue #381).
- Integração do Header em algumas das páginas (Issue #467 e Issue #488).
- Integração do Back-End do requisito 4.1.C com a sua respetiva Front-End (Issue #461).
- Integração do Back-End do requisito 3.C com a sua respetiva Front-End (Issue #533).

PM:

- Criação do ficheiro de perfil onde consta a minha informação inicial (PM/profiles/Joao_Melo.txt e issue #58).
- Atualização do ficheiro de perfil para incluir todas as informações sobre o meu desempenho ao longo do semestre (PM/profiles/Joao_Melo.txt e Issue #534).


PROC:

- Aprendizagem de Git/GitLab (Issue #5).
- Aprendizagem inicial de GitKraken (Issue #57).
- Aprendizagem da tecnologia Django e instalação da mesma (Issue #110).
- Aprendizagem do modelo C4 e os seus diversos níveis, pesquisa sobre tecnologias a usar para o desenvolvimento do site. (Issue #246).


REQ:

- Criação do user story relativo ao requisito 4.1.C (4.1.C Deverá ser possível visualizar o esforço total aplicado no projeto, por categoria (REQ, PM, DEV, TST...)) (Issue #121).
- Desenho do produto do requisito acima mencionado (Issue #309).
- Verificação do cumprimento do user story do requisito 4.1.D (Visualização da evolução do esforço aplicado no projeto, por categoria (REQ, PM, DEV, TST...)) (Issue #191).


Outras tarefas:

Papel principal no projeto: O meu papel principal no projeto foi de Arquiteto, participei na íntegra na realização da arquitetura do site. Também participei no desenvolvimento tecnologias de Back-End, nomeadamente, a implementação dos requisitos.
