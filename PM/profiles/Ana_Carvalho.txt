name: Ana Carvalho
email:uc2019213446@student.uc.pt
role: front-end /secretariado
idGitlab:aiccarvalho2008
nAluno: 2019213446
DES:
- Desenvolvimento da identidade visual (issue #141)
- Criação do Manual de normas da identidade visual (issue #164)
- Desenvolvimento dos layouts do site (issue #216,#269,#305)
- Decisão das convenções do css (issue #356)

DEV:
-Desenvolvimento das páginas do front-end dos Membros e Perfil (issue #394 #395)
-Implementação de CSS e da informação de back-end em Django Template Language da User Profile e Membros (issue #436,#516)

PM:
- Criação do ficheiro profile (issue #35)
- Realização de Atas (issue #97,#220,#350,#464)
- Atualização do ficheiro profile (issue #)

PROC:
- Aprendizagem de Git e GitLab (issue #17)
-Aprendizagem de Django e Python (issue #118)

REQ:
- Criação do requisito userstory correspondente ao requisito 1.E (issue #140)
- Verificação do userstory 3.C (issue #208)
- Desenho do produto do requisito 1.E (issue #370)

TST:
- Teste do requisito 2.C (issue #546)

Papel principal no projeto:
Nas Contribuições:
No Papel principal:
Fiz parte do Secretariado e fui Front_End Developer


