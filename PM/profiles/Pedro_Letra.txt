Nome: Pedro Miguel Cardoso Letra

Número de estudante: 2019210510

Email: uc2019210510@student.uc.pt

ID no GitLab: pmiguelcletra

Cargos: Front-end Developer / Quality Assurance Team

Contribuições:
DEV:
- Desenvolvimento do Front-end da página Commits (Issue #408)
- Junção do Front-End e Back-End do requisito 6.C (Issue #487)

PM:
- Criação do ficheiro profile (Issue #425)
- Atualização do ficheiro de perfil (Issue #548)

PROC:
- Aprender Git e GitLab (Issue #16)
- Aprendizagem Django (Issue #142)

QA:
- Verificação de criação dos User stories, instalação e aprendizagem de Django/ Python (Issue #173)
- Verificação de issues e analise estatística (Issue #282)
- Criação do ficheiro Template_Perfil (Issue #508)

REQ:
- Criação do user story: Deverá ser apresentada a árvore de ficheiros do repositório (Requisito 2.A) (Issue #145)
- Verificação do user story 4.2.E (Issue #258)
- Desenho do produto do requisito 2.A (Issue #349)
- Verificação do desenho do produto 1.E (Issue #473)
- Verificação do user story 1.H (Issue #198)
- Desenho do produto 1.C (Issue #469)
- Desenho do produto 2.D (Issue #475)
- Desenho do produto 5.B (Issue #479)

TST:
- Teste do Requisito 4.2.B (Issue #497)

Papel principal no projeto:
Nas primeiras semanas fiz apenas tarefas de aprendizagem e parte do controlo de qualidade, no restante tempo apoiei a equipa do Front-end e continuei a colaborar com o controlo de qualidade.
