Nome: Fábio Miguel Rodrigues Vaqueiro
Número de estudante: 2019222451
Email: uc2019222451@student.uc.pt
ID no GitLab: fabiomvaqueiro
Cargos: Backend Developer, DevOps
 
Contribuições:
 
DEV:
 - Funcionalidade de token para aceder aos repositórios GitLab de um utilizador e armazenamento destes dados numa Cookie para mais fácil acesso (Issue #381).
 - Iniciação dos conteúdos fundamentais a utilizar no website (Funcionamento com a API do gitlab recorrendo a Python-Gitlab) (Issue #382).
 - Desenvolvimento do Back-end do requisito 1.G (Issue #401)
 - Desenvolvimento do Back-end do requisito 3.A.2 (Issue #449)
 - Integração do Back-End do requisito 3.A.2 com a sua respetiva Front-End (Issue #507).
 - Integração do Back-End do requisito 1.G com a sua respetiva Front-End (Issue #535).
 

PM:
- Criação do ficheiro de perfil onde consta a minha informação inicial (PM/profiles/Fabio_Vaqueiro.txt e issue #42).
- Atualização do ficheiro de perfil para incluir todas as informações sobre o meu desempenho ao longo do semestre (PM/profiles/Fabio_Vaqueiro.txt e Issue #552).

 
PROC:
- Aprendizagem da tecnologia Django e instalação da mesma (Issue #130).
 

REQ:
- Criação do user story relativo ao requisito 4.1.D (4.1.D Visualização da evolução do esforço aplicado no projeto, por categoria (REQ, PM, DEV, TST...)) (Issue #131).
- Desenho do produto do requisito acima mencionado (Issue #330).
- Desenho do Produto 3.A.2 (Issue #448).


TST:
 - Verificação do cumprimento do user story do requisito 5.A (5.A Visualizar a timeline de issues ao longo do tempo) (Issue #211).
 - Verificação Desenho do Produto 2.E (Issue #342)
 - Verificação Desenho do Produto 6.B (Issue #343)
 - Teste do Requisito 4.1.B (Issue #494)

Devop:
 - Aprendizagem da configuração da Pipeline (Issue #235)
 - Configuração da Pipeline (Issue #288 e #369)
 - Deploy do Website (Issue #441)

Outras tarefas:

Papel principal no projeto: O meu papel principal no projeto foi desenvolvimento de Back-End, participei na íntegra na realização na implementação dos requisitos. Também participei ativamente no deploy do website e na configuração da pipeline.

