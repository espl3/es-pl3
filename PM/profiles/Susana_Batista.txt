Nome: Susana Batista
Número de estudante: 2019208639
Email: uc2019208639@student.uc.pt
ID no GitLab: susana.pires.batista
Cargos: front-end, secretariado

Contribuições:

ARCH:
- Estudo da identidade visual do site (issue #172)
- Desenvolvimento dos layouts do site (issues #214, #268, #303)
- Decisão das convenções de css a usar no site (issue #359)

DEV:
- Desenvolvimento do front-end do header, incluindo os requisitos 1B, 1F e 1H (issue #397)
- Desenvolvimento do front-end do requisito 1G (issue #503)
- Integração e junção com back-end do header nas páginas (issues #467, #488, #512)
- Junção com back-end do requisito 1G (issue #535)
- Junção com back-end do requisito 1F (issue #563)

PM:
- Criação do ficheiro profile (issue #37)
- Realização de atas (issues #23, #128, #185, #285, #409, #562)
- Colocação de atas no repositório (issue #515)
- Atualização do ficheiro profile (issue #517)

PROC:
- Aprendizagem de Git e GitLab (issue #22)
- Aprendizagem de Python (issue #78)
- Aprendizagem de Django (issue #115)

REQ:
- Criação do userstory relativo ao requisito 1F (issue #143)
- Verificação do user story 2B (issue #187)
- Desenho do produto do requisito 1F (issue #311)

TST:
- Teste do requisito 4.1A (issue #493)

Papel principal no projeto: fiz parte do secretariado, fui front-end developer
