Nome: Alexandre van Velze
Número de estudante: 2019216618
Email: uc2019216618@student.uc.pt
ID no GitLab: alexvvelze
Cargos: Back-end Leader / DevOps Team

Contribuições:

DEV:

- Desenvolvimento do conteúdo Back-End do requisito 4.2.B (Deverá ser possível ver a lista de contribuições por categoria (REQ, PM, DEV,...) de cada membro ativo.) (Issue #418).
- Desenvolvimento do conteúdo Back-End do requisito 4.2.C (Deverá ser possível visualizar o papel de cada membro ativo a partir da intensidade das suas contribuições por categoria.) (Issue #529).
- Iniciação dos conteúdos fundamentais a utilizar no website (Funcionamento com a API do gitlab recorrendo à biblioteca python-gitlab) (Issue #414).
- Implementação do token para aceder aos repositórios GitLab de um utilizador e armazenamento destes dados numa Cookie para tornar mais fácil o acesso (Issue #417).
- Conclusão da página de login através da junção entre Back-End e Front-End (Issue #442).
- Junção de requisitos e deployment dos mesmos (Issue #482).

PM:

- Criação e alteração do README.md na pasta PM e PM/profiles. (Issue #302).
- Criação do ficheiro de perfil onde consta a minha informação inicial (Issue #46).
- Colocação das atas no repositório (PM/minutes) (Issue #223).
- Atualização do ficheiro de perfil para incluir todas as informações sobre o meu trabalho ao longo do semestre (Issue #566).

PROC:

- Aprendizagem de Git/GitLab (Issue #8).
- Aprendizagem e instalação do GitKraken (Issue #31).
- Aprendizagem de Django (Issue #184).
- Procura de soluções para fazer deployment do Website (Issue #421).

REQ:

- Criação do user story relativo ao requisito 4.2.B (Deverá ser possível ver a lista de contribuições por categoria (REQ, PM, DEV,...) de cada membro ativo.) (Issue #126).
- Desenho do produto 4.2.B (Issue #310).
- Verificação Desenho do Produto 4.2.D (Deverá ser possível observar a lista (tabela) de commits de cada membro ativo.) (Issue #323).
- Verificação do User Story 3.B (Deverá ser possivel saber, por ficheiro, quais os issues que lhe estão associados.) (Issue #192).

TST: 

- Teste do Requisito 4.2.A (Issue #564).

Outras tarefas:

    - Criação e alteração do README.md principal do projeto de forma a informar os visitantes do contéudo do repositório (Issue #302).
    - Criação de vários ficheiros README.md em falta (Issue #568).

    Deployment:
        - Criacão e setup da maquina virtual no Cloud2 (Issue #229).
        - Deployment da aplicação na plataforma Heroku (Issue #435).
        - Alteração do deployment da aplicação para a máquina virtual fornecida na plataforma Cloud2 (Issue #506).



