name: Mário Guilherme De Almeida Martins Sequeira Lemos
email: uc2019216792@student.uc.pt
idGitlab: Mario_Lemos
nAluno: 2019216792
Cargos: Back-end Developer, Arquitetura, DevOps Team

Contribuições:

ARCH:

- Desenvolvimento da Arquitetura do site a partir do modelo C4 (issue #286 e #318).
- Revisão final da Arquitetura do site para verificar e modificar certas alterações feitas. Finalização do nível 4 do modelo C4 
(Issue #514).


DEV:

- Desenvolvimento do conteúdo Back-End do requisito 4.1.B (Deverá ser possível visualizar as contribuições por categoria (REQ, PM, DEV, TST...) de todos os membros ativos) (issue #420).
- Desenvolvimento do conteúdo Back-End do requisito 2.B (Os ficheiros apresentados em cada diretoria/pasta deverão ser visíveis (nome e extensão dos ficheiros)) (issue #557).
- Junção do Back-End e do Front-End da página dos membros referente aos requisitos 4.1.A, 4.1.B e 4.1.C (issue #461).
- Resolução do token e cookie para utilização nos requisitos seguintes (issue #380).
- Integração do Back-End do requisito 2.B (em conjunto com o 2.A e 2.C) com a sua respetiva Front-End (issue #504).
- Implementação do Header na página dos issues (issue #467).


PM:

- Criação do ficheiro de perfil onde consta a minha informação inicial (PM/profiles/Mario_Lemos.txt e issue #177).
- Atualização do ficheiro de perfil para incluir todas as informações sobre o meu desempenho ao longo do semestre (PM/profiles/Mario_Lemos.txt e issue #).

PROC:

-Aprender Git/Gitlab (issue #30).
-Aprendizagem dos básicos de Django (issue #178).

REQ:

- Criação do User Story do requisito 4.1.B (Deverá ser possível visualizar as contribuições por categoria (REQ, PM, DEV, TST...) de todos os membros ativos) (issue #133).
- Atualização do User Story do 4.1.B segundo as indicações do cliente (issue #274).
- Verificação do User Story do requisito 4.1.A (issue #194).
- Desenho do produto 4.1.B (issue #345).

TST:
- Teste do requisito 6.C (issue #560).


Outras Tarefas:

- Analise de Merge Requests e testes (issue #458).
- Esolha do requisito inicial a desenvolver (issue #76).
- Reuniões.

Pipeline:

- Aprendizagem da configuração da pipeline (issue #236).
- Multiplas tentativas de configuração da pipeline e Ci runners bem como a sua ligação a máquina virtual fornecida (issue #290 e #374).

Deployment:

- Deployment teste do website no Heroku. (issue #447).
- Deployment do StormLab para a máquina virtual fornecida na plataforma Cloud2 (issue #506).

Papel principal no Projeto:

- Fui Back-End developer, tendo também efetuado várias tarefas referentes a arquitetura. Efetuei também o deploy do website bem como a configuração da pipeline.



