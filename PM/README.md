# PM - Project Management

Project management related files are stored here.


## Folders

- [`PM/dash`](https://gitlab.com/espl3/es-pl3/-/tree/main/PM/dash/): Project dashboards are located here
- [`PM/minutes`](https://gitlab.com/espl3/es-pl3/-/tree/main/PM/minutes/): Redacted minutes are located here
- [`PM/profiles`](https://gitlab.com/espl3/es-pl3/-/tree/main/PM/profiles/): User profiles are located here
- [`PM/rsk`](https://gitlab.com/espl3/es-pl3/-/tree/main/PM/rsk/): Risks are managed here


