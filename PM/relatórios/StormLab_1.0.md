﻿

StormLab

by Softwork

Universidade De Coimbra | FCTUC

Engenharia de Software





1\_Equipa SoftWork

1.1 Projeto a ser testado : StormLab

1.2 Email de contacto : lopix23@gmail.com

2\_ID da versão com data

Versão 1.0 (04/12/2021)

3\_Link para a versão online

https://stormlab.herokuapp.com/

4\_Link para a lista de requisitos

https://gitlab.com/espl3/es-pl3/-/blob/main/README.md

5\_Lista de requisitos (por extenso) suportado pelo website

\_Identiﬁcador do Requisito: 1.A

Escolha do repositório GitLab do utilizador a analisar.

\_Identiﬁcador do Requisito: 4.1.A

Criação de uma página com a lista de membros ativos (efetuou

9

pelo menos 1 commit) do repositório.

\_Identiﬁcador do Requisito:4.1.B

Visualização das contribuições por categoria de todos os membros

1 9

ativos.

\_Identiﬁcador do Requisito: 4.1.D

Esforço total aplicado no projeto, por categoria .

1 9

\_Identiﬁcador do Requisito:4.2.B

Visualização da lista de contribuições por categoria .

1 8

\_Identiﬁcador do Requisito: 4.2.D

Observação da lista (tabela) de commits de cada membro ativo do

repositório .

4





\_Identiﬁcador do Requisito:4.2.E

Visualização da timeline de commits de cada membro ativo .

3

\_Identiﬁcador do Requisito: 5.A

Visualização da timeline de issues ao longo do tempo (abertos,

7

ativos e total).

\_Identiﬁcador do Requisito: 6.C

Visualização, por commit, dos ﬁcheiros alterados, assim como o

respetivo autor .

2 6

\_Identiﬁcador do Requisito: N/A

Realização do login com o token do GitLab do utilizador .

5

1

Por categorias entende-se os parâmetros: TODOS, ARCH, DES, DEV, ENV, PM, PROC, PROD,

QA, REC, TST

2 A visualização desta página encontra-se elaborada com código HTML básico.

3 Acessíveis através do url: https://stormlab.herokuapp.com/members/user/{username}/

{branch}/timeline/

4 Acessíveis através do url: https://stormlab.herokuapp.com/members/user/{username}/

{branch}/table/

5 Deve-se ir buscar o token pessoal ao GitLab. Para isso deve clicar em

Preferences>Access Token dar um nome ao mesmo e selecionar a scope “api”.

6 Acessíveis através do url: https://stormlab.herokuapp.com/commits/{page\_number}

7 Acessíveis através do url: https://stormlab.herokuapp.com/issues/{open/closed}

8 Acessíveis através do url: https://stormlab.herokuapp.com/commits/{category}

9 Acessíveis através do url: https://stormlab.herokuapp.com/members





Membros ativos da equipa SoftWork

\- Alexandre da Silva van Velze 2019216618

\- Ana Gil Oliveira Ferreira 2018302843

\- Ana Isabel da Cunha Carvalho 2019213446

\- António Pedro Correia 2019216646

\- Bárbara da Palma Teixeira 2019212062

\- Beatriz dos Reis Ferreira 2019226233

\- Bruno Miguel Santos Marques 2018278019

\- David Forte da Ressurreição 2019219749

\- Dumitru Brailean 2019226123

\- Fábio Miguel Rodrigues Vaqueiro 2019222451

\- Filipa Mendes Nogueira 2019212940

\- Francisco João Romão Pires 2019201225

\- João António da Silva Melo 2019216747

\- João Carlos Borges Silva 2019216753

\- João Lopes Teixeira Monteiro 2019216764

\- João Maria Veloso Cristóvão Santos 2019210135

\- Luís Alexandre Gomes Gonçalves 2019228021

\- Mário Guilherme de Almeida Martins Sequeira Lemos 2019216792

\- Miguel António Gabriel de Almeida Faria 2019216809

\- Nathalie Andreina Pinto Jennings 2019226002

\- Pedro Afonso Ferreira Lopes Martins 2019216826

\- Pedro Miguel Cardoso Letra 2019210510

\- Susana Pires Batista 2019208639


