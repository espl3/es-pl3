﻿![](Aspose.Words.d77873e2-4414-4442-bc3b-046fe9bf65a4.001.png)![](Aspose.Words.d77873e2-4414-4442-bc3b-046fe9bf65a4.002.png)![](Aspose.Words.d77873e2-4414-4442-bc3b-046fe9bf65a4.003.png)![](Aspose.Words.d77873e2-4414-4442-bc3b-046fe9bf65a4.004.png)![](Aspose.Words.d77873e2-4414-4442-bc3b-046fe9bf65a4.005.png)![](Aspose.Words.d77873e2-4414-4442-bc3b-046fe9bf65a4.006.png)![](Aspose.Words.d77873e2-4414-4442-bc3b-046fe9bf65a4.007.png)

Universidade De Coimbra | FCTUC Engenharia de Software

**1\_Equipa SoftWork** 

1. Projeto a ser testado: Git Buster 
1. Turma: PL4 

**2\_ID da versão e data** 

1. ID versão: fb4857c5 
1. Data: 04/12/2021 

**3\_Link para a versão online**  

[http://10.17.0.212:8080/gitbuster/ ](http://10.17.0.212:8080/gitbuster/)

**4\_Link para a lista de requisitos** 

https://gitlab.com/ESPL4/GitBuster/- /blob/811c76bc1c5ded789a7337ee31fc504e8b0a2d6f/REQ/Requisitos.md 

**5\_Lista de requisitos a ser testados** 

REQ-US#16: Como utilizador, quero que apenas utilizadores autenticados possam aceder a informação interna do projeto, para que o acesso seja mais controlado. REQ-US#18: Como utilizador, quero ter uma chave token, para ter acesso à aplicação. 

REQ-US#19: Como utilizador, quero puder fazer o meu registo, para aceder ao projeto. 

REQ-US#20: Como utilizador, quero visualizar a árvore do repositório, para ter conhecimento geral da estrutura. 

**6\_Testes Realizados** 

**6.1** 

O projeto entregue na data referida não possuía os requisitos acima mencionados implementados  na  sua  totalidade,  sendo  este  facto  confirmado  pela  gestora  do projeto, Amanda Menezes, que transmitiu a informação de que os requisitos não dispunham  das  funcionalidades  de  requests  para  obter  a  vista  do  repositório, possuindo apenas o seu esqueleto. 

O trabalho enviado apenas aparentava ter o front-end dos requisitos, não estando os mesmos concluídos ou funcionais. Deste modo, a análise do projeto para atribuição de tarefas de testes, pelo gestor do projeto, não foi realizada, sendo a análise trivial. 

**6.2** 

O projeto deployed não possui as condições para ser aprovado, pelas razões acima mencionadas. 

**6.3** 



|**Requisito** |**Observações** |
| - | - |
|REQ-US#16 |Encontrava-se  presente  o  front-end deste  requisito,  contudo  a funcionalidade de login não funcionava |


|**Requisito** |**Observações** |
| - | - |
|REQ-US#18 |Encontrava-se presente o front-end deste requisito, contudo qualquer inserção no campo token devolve o debug. |


|**Requisito** |**Observações** |
| - | - |
|REQ-US#19 |Encontrava-se  presente  o  front-end deste  requisito,  contudo  a funcionalidade  de  register  não funcionava. |


|**Requisito** |**Observações** |
| - | - |
|REQ-US#20 |Encontrava-se  presente  o  front-end deste requisito. |
**Membros ativos da equipa SoftWork**

- Alexandre da Silva van Velze 2019216618 
- Ana Gil Oliveira Ferreira 2018302843 
- Ana Isabel da Cunha Carvalho 2019213446 
- António Pedro Correia 2019216646 
- Bárbara da Palma Teixeira 2019212062 
- Beatriz dos Reis Ferreira 2019226233 
- Bruno Miguel Santos Marques 2018278019 
- David Forte da Ressurreição 2019219749 
- Dumitru Brailean 2019226123 
- Fábio Miguel Rodrigues Vaqueiro 2019222451 
- Filipa Mendes Nogueira 2019212940 
- Francisco João Romão Pires 2019201225 
- João António da Silva Melo 2019216747 
- João Carlos Borges Silva 2019216753 
- João Lopes Teixeira Monteiro 2019216764 
- João Maria Veloso Cristóvão Santos 2019210135 
- Luís Alexandre Gomes Gonçalves 2019228021 
- Mário Guilherme de Almeida Martins Sequeira Lemos 2019216792 
- Miguel António Gabriel de Almeida Faria 2019216809 
- Nathalie Andreina Pinto Jennings 2019226002 
- Pedro Afonso Ferreira Lopes Martins 2019216826 
- Pedro Miguel Cardoso Letra 2019210510 
- Susana Pires Batista 2019208639 
